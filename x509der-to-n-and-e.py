#! /usr/bin/env python3

import sys
from pathlib import Path
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa

if len(sys.argv)<2:
    print("Erwarte als erstes Argument den Dateinamen eines X.509-Zertifikats (im DER-Format)\n"+
          "und gebe den RSA-Modulus (n) und den öffentlichen Exponenten (e) des im Zertifikat\n"+
          "bestätigten Schlüssel im Hexadezimalformat aus."
         )
    sys.exit(1)

my_file=Path(sys.argv[1]);

if my_file.exists() and my_file.is_file():
    der_data=my_file.read_bytes()
    cert = x509.load_der_x509_certificate(der_data, default_backend())
    #print("Subject={}\nSN={}".format(cert.subject, cert.serial_number))
    public_key = cert.public_key()
    if isinstance(public_key, rsa.RSAPublicKey):
        modulus=public_key.public_numbers().n
        e      =public_key.public_numbers().e
        print("n=0x{0:x}".format(modulus))
        print("e=0x{0:x}".format(e))
    else:
        # bspw. ECC-Schluessel im Zertifikat
        print("Der im Zertifikat bestaetigete Schluessel ist kein RSA-Schluessel.")
        sys.exit(1)

else:
    print('Kann Datei "{}" nicht lesen.'.format(sys.argv[1]))
    sys.exit(1)


