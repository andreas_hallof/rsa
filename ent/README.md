# Thema einfache Zufallstests auf einen öffentlichen RSA-Schlüssel anwenden

    $ ./rsa-ent.py

    ____ ____ ____    ____ ____ _  _ _    _  _ ____ ____ ____ ____ _       ____ 
    |__/ [__  |__|    [__  |    |__| |    |  | |___ [__  [__  |___ |       |__| 
    |  \ ___] |  |    ___] |___ |  | |___ |__| |___ ___] ___] |___ |___    |  | 
                                                                                

    p=fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0012d
    q=ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff4d
    n=p*q=fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0007a000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b2f2d89
    2048
    b'00fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0007a000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b2f2d89'
    entropy: 1.1968295844322032
    ____ ____ ____    ____ ____ _  _ _    _  _ ____ ____ ____ ____ _       ___  
    |__/ [__  |__|    [__  |    |__| |    |  | |___ [__  [__  |___ |       |__] 
    |  \ ___] |  |    ___] |___ |  | |___ |__| |___ ___] ___] |___ |___    |__] 
                                                                                

    p=610a261395a9bf860c6a5972de9a5daa4aff865638aef4eff55dcedd0dd8b7f4f7f46c603965092542dbb94cf207eb7c6a2a427714610cbb105b238f4c15a7de3edcd4625a47c527163fda514729793657834469eb9a2a4595a4f58221e28c30358b0229cc83453b8b18649c584049c05937e59f5675d83e7c7d4675963ea965
    q=cbaa46ea2abd75ae0651e21343a5b8e7f187df23a930c207344c64d5e82e957f9f2f3a9d39127b5ba9c04d5107869a227dcad7428590d60a200fa7a0aabdd3ad4243d160848c727375a668f688f86f40b2871def5924f1e7cf66a59f79c7172429a0f1dabf2ef0a0318321e43b7808f9a70ae4355ca04ac88db44bc229955913
    n=p*q=4d3397d05a92a1c4a019bebb611760ff6f03edbb3311cdcd7606e4bdd0c5f9552b3bff461840ad6b89ef2d98c045bd13686d7ce0e0926a1d7656f1604ced3d95958d9b57c40940b1e1038663e8ce74adfb38a5501555b2a4af2cc0fcaccc54330e85c9173c4f49021b5c8621555141a074196967ae6ebfa3f1785dfc8a5fb8e623e8a524077af0d6e4225a4333435a0880cc1aec2e8775347ecc5402b2be441e3000d87f8102071d48a075a877b4d9b0be443d8e03bba0a752047d2210f27cdd2db2a8f9cc6f7919d91e24cc5f6b4cc7eff251388b94ad2a3f62a966a9ec6ed928ff2cc78ad6d6c2a5190d21a22f6c68e93bd3114348d381956c8d19b453af7f
    2047 256
    b'4d3397d05a92a1c4a019bebb611760ff6f03edbb3311cdcd7606e4bdd0c5f9552b3bff461840ad6b89ef2d98c045bd13686d7ce0e0926a1d7656f1604ced3d95958d9b57c40940b1e1038663e8ce74adfb38a5501555b2a4af2cc0fcaccc54330e85c9173c4f49021b5c8621555141a074196967ae6ebfa3f1785dfc8a5fb8e623e8a524077af0d6e4225a4333435a0880cc1aec2e8775347ecc5402b2be441e3000d87f8102071d48a075a877b4d9b0be443d8e03bba0a752047d2210f27cdd2db2a8f9cc6f7919d91e24cc5f6b4cc7eff251388b94ad2a3f62a966a9ec6ed928ff2cc78ad6d6c2a5190d21a22f6c68e93bd3114348d381956c8d19b453af7f'
    entropy: 7.141454273518899
    ____ ____ ____    ____ ____ _  _ _    _  _ ____ ____ ____ ____ _       ____ 
    |__/ [__  |__|    [__  |    |__| |    |  | |___ [__  [__  |___ |       |    
    |  \ ___] |  |    ___] |___ |  | |___ |__| |___ ___] ___] |___ |___    |___ 
                                                                                

    Test: durch einen unglücklichen Fehler werden die letzten 512 Bit (64 Byte) von n auf Null gesetzt.
    256
    b'4d3397d05a92a1c4a019bebb611760ff6f03edbb3311cdcd7606e4bdd0c5f9552b3bff461840ad6b89ef2d98c045bd13686d7ce0e0926a1d7656f1604ced3d95958d9b57c40940b1e1038663e8ce74adfb38a5501555b2a4af2cc0fcaccc54330e85c9173c4f49021b5c8621555141a074196967ae6ebfa3f1785dfc8a5fb8e623e8a524077af0d6e4225a4333435a0880cc1aec2e8775347ecc5402b2be441e3000d87f8102071d48a075a877b4d9b0be443d8e03bba0a752047d2210f27cdd00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'
    entropy: 6.020554217335962
    ____ ____ ____    ____ ____ _  _ _    _  _ ____ ____ ____ ____ _       ___  
    |__/ [__  |__|    [__  |    |__| |    |  | |___ [__  [__  |___ |       |  \ 
    |  \ ___] |  |    ___] |___ |  | |___ |__| |___ ___] ___] |___ |___    |__/ 
                                                                                

    Test: durch einen unglücklichen Fehler werden die ersten 256 Byte der base64-Kodierung von n verwendet.
    344 b'TTOX0FqSocSgGb67YRdg/28D7bszEc3NdgbkvdDF+VUrO/9GGECta4nvLZjARb0TaG184OCSah12VvFgTO09lZWNm1fECUCx4QOGY+jOdK37OKVQFVWypK8swPyszFQzDoXJFzxPSQIbXIYhVVFBoHQZaWeubr+j8Xhd/IpfuOYj6KUkB3rw1uQiWkMzQ1oIgMwa7C6HdTR+zFQCsr5EHjAA2H+BAgcdSKB1qHe02bC+RD2OA7ugp1IEfSIQ8nzdLbKo+cxveRnZHiTMX2tMx+/yUTiLlK0qP2KpZqnsbtko/yzHitbWwqUZDSGiL2xo6TvTEUNI04GVbI0ZtFOvfw=='
    256 b'TTOX0FqSocSgGb67YRdg/28D7bszEc3NdgbkvdDF+VUrO/9GGECta4nvLZjARb0TaG184OCSah12VvFgTO09lZWNm1fECUCx4QOGY+jOdK37OKVQFVWypK8swPyszFQzDoXJFzxPSQIbXIYhVVFBoHQZaWeubr+j8Xhd/IpfuOYj6KUkB3rw1uQiWkMzQ1oIgMwa7C6HdTR+zFQCsr5EHjAA2H+BAgcdSKB1qHe02bC+RD2OA7ugp1IEfSIQ8nzd'
    entropy: 5.809082988038221
    ____ ____ ____    ____ ____ _  _ _    _  _ ____ ____ ____ ____ _       ____ 
    |__/ [__  |__|    [__  |    |__| |    |  | |___ [__  [__  |___ |       |___ 
    |  \ ___] |  |    ___] |___ |  | |___ |__| |___ ___] ___] |___ |___    |___ 
                                                                                

    Test: durch einen unglücklichen Fehler werden Bitfolgen im Schlüssel wiederholt:

    -----BEGIN PUBLIC KEY-----
    MIIBIzANBgkqhkiG9w0BAQEFAAOCARAAMIIBCwKCAQIA8h
    O1l3lbPR8A4sSmiGpMLg/x07WXeVs9HwDixKaIakwuD/HTtZd5Wz0fAOLEpohqTC4P8d
    O1l3lbPR8A4sSmiGpMLg/x07WXeVs9HwDixKaIakwuD/HTtZd5Wz0fAOLEpohqTC4P8d
    O1l3lbPR8A4sSmiGpMLg/x07WXeVs9Hxeb
            8A4sSmiGpMLg/x07WXeVs9HwDixKaIakwuD/HTtZd5Wz0fAOLEpohqTC4P8d
    O1l3lbPR8A4sSmiGpMLg/x07WXeVs9HwDixKaIakwuD/HTtZd5Wz0fAOLEpohqTC4P8d
    O1l3lbPR8A4sSmiGpMLg/x07WXeVs9HwDixKaIakw
    ssCAwEAAQ==
    -----END PUBLIC KEY-----

    257 b'f213b597795b3d1f00e2c4a6886a4c2e0ff1d3b597795b3d1f00e2c4a6886a4c2e0ff1d3b597795b3d1f00e2c4a6886a4c2e0ff1d3b597795b3d1f00e2c4a6886a4c2e0ff1d3b597795b3d1f00e2c4a6886a4c2e0ff1d3b597795b3d1f00e2c4a6886a4c2e0ff1d3b597795b3d1f00e2c4a6886a4c2e0ff1d3b597795b3d1f179bf00e2c4a6886a4c2e0ff1d3b597795b3d1f00e2c4a6886a4c2e0ff1d3b597795b3d1f00e2c4a6886a4c2e0ff1d3b597795b3d1f00e2c4a6886a4c2e0ff1d3b597795b3d1f00e2c4a6886a4c2e0ff1d3b597795b3d1f00e2c4a6886a4c2e0ff1d3b597795b3d1f00e2c4a6886a4c2e0ff1d3b597795b3d1f00e2c4a6886a4c2cb'
    entropy: 5.168933151964051

# Openssl die Entropie-Quellen klauen

## Schluessel erzeugen 1

    $ strace openssl genrsa -out 3 2048
    execve("/usr/bin/openssl", ["openssl", "genrsa", "-out", "3", "2048", "2"], 0x7fffb6b6f3e8 /* 22 vars */) = 0
    brk(NULL)                               = 0x55bc221cd000
    access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
    access("/etc/ld.so.preload", R_OK)      = -1 ENOENT (No such file or directory)
    openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
    fstat(3, {st_mode=S_IFREG|0644, st_size=22686, ...}) = 0
    mmap(NULL, 22686, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f8ff2bc4000
    close(3)                                = 0
    access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
    openat(AT_FDCWD, "/usr/lib/x86_64-linux-gnu/libssl.so.1.1", O_RDONLY|O_CLOEXEC) = 3
    read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\240w\1\0\0\0\0\0"..., 832) = 832
    fstat(3, {st_mode=S_IFREG|0644, st_size=433760, ...}) = 0
    mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f8ff2bc2000

Quelle 1 ...

    openat(AT_FDCWD, "/home/osboxes/.rnd", O_RDONLY) = 4 
    fstat(4, {st_mode=S_IFREG|0600, st_size=1024, ...}) = 0 
    futex(0x7f8ff273782c, FUTEX_WAKE_PRIVATE, 2147483647) = 0 
    futex(0x7f8ff2737394, FUTEX_WAKE_PRIVATE, 2147483647) = 0 
    fstat(4, {st_mode=S_IFREG|0600, st_size=1024, ...}) = 0 
    read(4, "h\266\374\37\22\210\300\1!Fr\345\252]\237\7\"U\255+!\37\6\200\212\363\232\303\321\10\21\221"..., 4096) = 1024
    read(4, "", 4096)                       = 0 
    close(4)                                = 0 
    write(2, "Generating RSA private key, 2048"..., 50Generating RSA private key, 2048 bit long modulus) = 50

Quelle 2 ...

    getpid()                                = 1816
    futex(0x7f8ff2737230, FUTEX_WAKE_PRIVATE, 2147483647) = 0
    getpid()                                = 1816
    openat(AT_FDCWD, "/dev/urandom", O_RDONLY|O_NOCTTY|O_NONBLOCK) = 4
    fstat(4, {st_mode=S_IFCHR|0666, st_rdev=makedev(1, 9), ...}) = 0
    poll([{fd=4, events=POLLIN}], 1, 10)    = 1 ([{fd=4, revents=POLLIN}])
    read(4, "Pk\223\267\225.s\212/\201\210\266%\327\337\35\0p\313o\256\247\n\252\232J\2\257\313\335\276o", 32) = 32
    close(4)                                = 0

Quelle 3 ...


    getuid()                                = 1000
    write(2, ".", 1.)                        = 1
    getpid()                                = 1816
    getpid()                                = 1816
    write(2, ".", 1.)                        = 1
    getpid()                                = 1816
    getpid()                                = 1816
    write(2, ".", 1.)                        = 1
    getpid()                                = 1816
    getpid()                                = 1816
    write(2, ".", 1.)                        = 1
    getpid()                                = 1816
    getpid()                                = 1816
    getpid()                                = 1816
    getpid()                                = 1816
    write(2, ".", 1.)                        = 1
    getpid()                                = 1816
    getpid()                                = 1816
    getpid()                                = 1816
    write(2, ".", 1.)                        = 1
    getpid()                                = 1816
    getpid()                                = 1816
    write(2, ".", 1.)                        = 1
    getpid()                                = 1816
    getpid()                                = 1816
    write(2, ".", 1.)                        = 1
    getpid()                                = 1816

## Die Entropie-Quellen klauen

### Quelle 1

    $ ls -l /dev/urandom* /dev/zero
    lrwxrwxrwx 1 root root    4 Aug 11 07:13 /dev/urandom -> zero
    crw-rw-rw- 1 root root 1, 9 Aug 11 06:59 /dev/urandom.ori
    crw-rw-rw- 1 root root 1, 5 Aug 11 06:59 /dev/zero

### Quelle 2

    $ ls -l .rnd 
    -rw------- 1 osboxes osboxes 1024 Aug 11 18:39 .rnd
    $ xxd .rnd | head -2
    00000000: 377b 897a ca18 b79d c184 bafe bd3d 52c6  7{.z.........=R.
    00000010: ca7b 17c0 3f20 9df2 3c0d 5fcb 701b 0313  .{..? ..<._.p...
    $ dd if=/dev/zero of=.rnd bs=1 count=1024
    $ xxd .rnd | head -2
    00000000: 0000 0000 0000 0000 0000 0000 0000 0000  ................
    00000010: 0000 0000 0000 0000 0000 0000 0000 0000  ................

### Quelle 3

Timinginformation zwischen syscalls (getpid())?

## Schlüssel erzeugen 2

    $ strace openssl genrsa -out 8 2048
    execve("/usr/bin/openssl", ["openssl", "genrsa", "-out", "8", "2048"], 0x7ffec08254e0 /* 23 vars */) = 0
    brk(NULL)                               = 0x558ded7df000
    access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
    access("/etc/ld.so.preload", R_OK)      = -1 ENOENT (No such file or directory)
    openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
    fstat(3, {st_mode=S_IFREG|0644, st_size=22686, ...}) = 0
    mmap(NULL, 22686, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f03511e7000
    close(3)                                = 0
    access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
    openat(AT_FDCWD, "/usr/lib/x86_64-linux-gnu/libssl.so.1.1", O_RDONLY|O_CLOEXEC) = 3
    read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\240w\1\0\0\0\0\0"..., 832) = 832
    fstat(3, {st_mode=S_IFREG|0644, st_size=433760, ...}) = 0
    mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f03511e5000
    mmap(NULL, 2529304, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f0350d5c000
    mprotect(0x7f0350dbc000, 2097152, PROT_NONE) = 0
    mmap(0x7f0350fbc000, 40960, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x60000) = 0x7f0350fbc000
    close(2)                                = 0
    access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
    openat(AT_FDCWD, "/usr/lib/x86_64-linux-gnu/libcrypto.so.1.1", O_RDONLY|O_CLOEXEC) = 3
    read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\0\220\6\0\0\0\0\0"..., 832) = 832
    fstat(3, {st_mode=S_IFREG|0644, st_size=2575848, ...}) = 0
    mmap(NULL, 4685184, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f03508e4000
    mprotect(0x7f0350b31000, 2097152, PROT_NONE) = 0
    mmap(0x7f0350d31000, 163840, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x24d000) = 0x7f0350d31000
    mmap(0x7f0350d59000, 11648, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f0350d59000
    close(3)                                = 0
    access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
    openat(AT_FDCWD, "/lib/x86_64-linux-gnu/libpthread.so.0", O_RDONLY|O_CLOEXEC) = 3
    read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0000b\0\0\0\0\0\0"..., 832) = 832
    fstat(3, {st_mode=S_IFREG|0755, st_size=144976, ...}) = 0
    mmap(NULL, 2221184, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f03506c5000
    mprotect(0x7f03506df000, 2093056, PROT_NONE) = 0
    mmap(0x7f03508de000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x19000) = 0x7f03508de000
    mmap(0x7f03508e0000, 13440, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f03508e0000
    close(3)                                = 0
    access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
    openat(AT_FDCWD, "/lib/x86_64-linux-gnu/libc.so.6", O_RDONLY|O_CLOEXEC) = 3
    read(3, "\177ELF\2\1\1\3\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\260\34\2\0\0\0\0\0"..., 832) = 832
    fstat(3, {st_mode=S_IFREG|0755, st_size=2030544, ...}) = 0
    mmap(NULL, 4131552, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f03502d4000
    mprotect(0x7f03504bb000, 2097152, PROT_NONE) = 0
    mmap(0x7f03506bb000, 24576, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x1e7000) = 0x7f03506bb000
    mmap(0x7f03506c1000, 15072, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f03506c1000
    close(3)                                = 0
    access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
    openat(AT_FDCWD, "/lib/x86_64-linux-gnu/libdl.so.2", O_RDONLY|O_CLOEXEC) = 3
    read(3, "\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0P\16\0\0\0\0\0\0"..., 832) = 832
    fstat(3, {st_mode=S_IFREG|0644, st_size=14560, ...}) = 0
    mmap(NULL, 2109712, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f03500d0000
    mprotect(0x7f03500d3000, 2093056, PROT_NONE) = 0
    mmap(0x7f03502d2000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x2000) = 0x7f03502d2000
    close(3)                                = 0
    mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f03511e3000
    arch_prctl(ARCH_SET_FS, 0x7f03511e41c0) = 0
    mprotect(0x7f03506bb000, 16384, PROT_READ) = 0
    mprotect(0x7f03502d2000, 4096, PROT_READ) = 0
    mprotect(0x7f03508de000, 4096, PROT_READ) = 0
    mprotect(0x7f0350d31000, 122880, PROT_READ) = 0
    mprotect(0x7f0350fbc000, 16384, PROT_READ) = 0
    mprotect(0x558ded120000, 16384, PROT_READ) = 0
    mprotect(0x7f03511ed000, 4096, PROT_READ) = 0
    munmap(0x7f03511e7000, 22686)           = 0
    set_tid_address(0x7f03511e4490)         = 2430
    set_robust_list(0x7f03511e44a0, 24)     = 0
    rt_sigaction(SIGRTMIN, {sa_handler=0x7f03506cacb0, sa_mask=[], sa_flags=SA_RESTORER|SA_SIGINFO, sa_restorer=0x7f03506d7890}, NULL, 8) = 0
    rt_sigaction(SIGRT_1, {sa_handler=0x7f03506cad50, sa_mask=[], sa_flags=SA_RESTORER|SA_RESTART|SA_SIGINFO, sa_restorer=0x7f03506d7890}, NULL, 8) = 0
    rt_sigprocmask(SIG_UNBLOCK, [RTMIN RT_1], NULL, 8) = 0
    prlimit64(0, RLIMIT_STACK, NULL, {rlim_cur=8192*1024, rlim_max=RLIM64_INFINITY}) = 0
    brk(NULL)                               = 0x558ded7df000
    brk(0x558ded800000)                     = 0x558ded800000
    futex(0x7f0350d5a278, FUTEX_WAKE_PRIVATE, 2147483647) = 0
    futex(0x7f0350d5a124, FUTEX_WAKE_PRIVATE, 2147483647) = 0
    rt_sigaction(SIGPIPE, {sa_handler=SIG_IGN, sa_mask=[PIPE], sa_flags=SA_RESTORER|SA_RESTART, sa_restorer=0x7f0350312f20}, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0
    futex(0x7f0350d5a21c, FUTEX_WAKE_PRIVATE, 2147483647) = 0
    futex(0x7f0350d586ec, FUTEX_WAKE_PRIVATE, 2147483647) = 0
    futex(0x7f0350d5a0bc, FUTEX_WAKE_PRIVATE, 2147483647) = 0
    futex(0x7f0350d5a0b0, FUTEX_WAKE_PRIVATE, 2147483647) = 0
    futex(0x7f0350d5a26c, FUTEX_WAKE_PRIVATE, 2147483647) = 0
    futex(0x7f0350d5a214, FUTEX_WAKE_PRIVATE, 2147483647) = 0
    openat(AT_FDCWD, "/usr/lib/ssl/openssl.cnf", O_RDONLY) = 3
    fstat(3, {st_mode=S_IFREG|0644, st_size=10771, ...}) = 0
    read(3, "#\n# OpenSSL example configuratio"..., 4096) = 4096
    read(3, "ape crash on BMPStrings or UTF8S"..., 4096) = 4096
    read(3, " is omitted\n# the certificate ca"..., 4096) = 2579
    read(3, "", 4096)                       = 0
    close(3)                                = 0
    futex(0x7f0350d5a248, FUTEX_WAKE_PRIVATE, 2147483647) = 0
    sysinfo({uptime=10904, loads=[2144, 768, 32], totalram=1033342976, freeram=427261952, sharedram=991232, bufferram=37064704, totalswap=6442446848, freeswap=6442446848, procs=108, totalhigh=0, freehigh=0, mem_unit=1}) = 0
    openat(AT_FDCWD, "8", O_WRONLY|O_CREAT|O_TRUNC, 0600) = 3
    fcntl(3, F_GETFL)                       = 0x8001 (flags O_WRONLY|O_LARGEFILE)
    getuid()                                = 1000
    geteuid()                               = 1000
    getgid()                                = 1000
    getegid()                               = 1000
    openat(AT_FDCWD, "/home/osboxes/.rnd", O_RDONLY) = 4
    fstat(4, {st_mode=S_IFREG|0600, st_size=1024, ...}) = 0
    futex(0x7f0350d5a82c, FUTEX_WAKE_PRIVATE, 2147483647) = 0
    futex(0x7f0350d5a394, FUTEX_WAKE_PRIVATE, 2147483647) = 0
    fstat(4, {st_mode=S_IFREG|0600, st_size=1024, ...}) = 0
    read(4, "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"..., 4096) = 1024

OK, Entropie-Quelle 1 ist damit weg.

    read(4, "", 4096)                       = 0
    close(4)                                = 0
    write(2, "Generating RSA private key, 2048"..., 50Generating RSA private key, 2048 bit long modulus
    ) = 50
    getpid()                                = 2430
    futex(0x7f0350d5a230, FUTEX_WAKE_PRIVATE, 2147483647) = 0
    getpid()                                = 2430
    openat(AT_FDCWD, "/dev/urandom", O_RDONLY|O_NOCTTY|O_NONBLOCK) = 4
    fstat(4, {st_mode=S_IFCHR|0666, st_rdev=makedev(1, 5), ...}) = 0
    poll([{fd=4, events=POLLIN}], 1, 10)    = 1 ([{fd=4, revents=POLLIN}])
    read(4, "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 32) = 32

OK, Entropie-Quelle 2 ist damit weg.

    close(4)                                = 0
    getuid()                                = 1000
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    write(2, "+", 1+)                        = 1
    getpid()                                = 2430
    write(2, "+", 1+)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, "+", 1+)                        = 1
    write(2, "\n", 1
    )                       = 1
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    getpid()                                = 2430
    write(2, ".", 1.)                        = 1
    getpid()                                = 2430
    write(2, "+", 1+)                        = 1
    getpid()                                = 2430
    write(2, "+", 1+)                        = 1
    getpid()                                = 2430
    write(2, "+", 1+)                        = 1
    write(2, "\n", 1)                       = 1
    getuid()                                = 1000
    geteuid()                               = 1000
    getgid()                                = 1000
    getegid()                               = 1000
    stat("/home/osboxes/.rnd", {st_mode=S_IFREG|0600, st_size=1024, ...}) = 0
    openat(AT_FDCWD, "/home/osboxes/.rnd", O_WRONLY|O_CREAT, 0600) = 4
    fcntl(4, F_GETFL)                       = 0x8001 (flags O_WRONLY|O_LARGEFILE)
    chmod("/home/osboxes/.rnd", 0600)       = 0
    getpid()                                = 2430
    fstat(4, {st_mode=S_IFREG|0600, st_size=1024, ...}) = 0
    write(4, "\276\215c;ew\3357\301\300\370\222\250\255\362+\215\320$\220\343\311/m\332\364|\312D\27\323\325"..., 1024) = 1024
    close(4)                                = 0
    write(2, "e is 65537 (0x010001)\n", 22e is 65537 (0x010001)
    ) = 22
    fstat(3, {st_mode=S_IFREG|0600, st_size=0, ...}) = 0
    write(3, "-----BEGIN RSA PRIVATE KEY-----\n"..., 1679) = 1679
    close(3)                                = 0
    exit_group(0)                           = ?
    +++ exited with 0 +++
    $ xxd .rnd | head -2
    00000000: be8d 633b 6577 dd37 c1c0 f892 a8ad f22b  ..c;ew.7.......+
    00000010: 8dd0 2490 e3c9 2f6d daf4 7cca 4417 d3d5  ..$.../m..|.D...

