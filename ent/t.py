#! /usr/bin/env python3

from hashlib import sha256
from binascii import hexlify
import secrets, math, sys, pyfiglet, shutil, base64
from gmpy2 import next_prime
from collections import Counter


def bytes_needed(n):
    if n == 0:
        return 1
    return int(math.log(n, 256)) + 1

def i2osp(integer, size=4):
  return ''.join([chr((integer >> (8 * i)) & 0xFF) for i in reversed(range(size))])

def banner(s):
    if not s:
        return 
    #rows, columns = os.popen('stty size', 'r').read().split()
    x=shutil.get_terminal_size((80, 24))
    print(pyfiglet.Figlet(font='cybermedium', width=int(x.columns)).renderText(s))

# experimenting with the entropy-measure-function from
# https://rosettacode.org/wiki/Entropy#Python
def entropy(s):
    p, lns = Counter(s), float(len(s))
    return -sum( count/lns * math.log(count/lns, 2) for count in p.values())

B=100; i=2; primes=[ ];
while i < B:
    primes.append(i)
    i=int(next_prime(i))
print("B={}".format(B))
print(len(primes), ":", primes)

primes=[ 2, 3, 5 ];
x=float(1)
for i in primes:
    print(x)
    x=x-1/float(i)

print(x)

