#! /usr/bin/env python3

from hashlib import sha256
from binascii import hexlify
import secrets, math, sys, pyfiglet, shutil, base64
from gmpy2 import next_prime
from collections import Counter


def bytes_needed(n):
    if n == 0:
        return 1
    return int(math.log(n, 256)) + 1

def i2osp(integer, size=4):
  return ''.join([chr((integer >> (8 * i)) & 0xFF) for i in reversed(range(size))])

def banner(s):
    if not s:
        return 
    #rows, columns = os.popen('stty size', 'r').read().split()
    x=shutil.get_terminal_size((80, 24))
    print(pyfiglet.Figlet(font='cybermedium', width=int(x.columns)).renderText(s))

# experimenting with the entropy-measure-function from
# https://rosettacode.org/wiki/Entropy#Python
def entropy(s):
    p, lns = Counter(s), float(len(s))
    return -sum( count/lns * math.log(count/lns, 2) for count in p.values())

banner("RSA Schluessel a")
p=2**1024-2**20
p=next_prime(p)
print("p={0:x}".format(p))

q=2**1024-2**10
q=next_prime(q)
print("q={0:x}".format(q))

n=int(p*q)
print("n=p*q={0:x}".format(n))
print(len(bin(n))-2)

n_encoded=n.to_bytes(bytes_needed(n), 'big')
print(hexlify(n_encoded))
print("entropy: {}".format(entropy(n_encoded)))


banner("RSA Schluessel b")
p=secrets.randbits(1024)
p=next_prime(p)
print("p={0:x}".format(p))

q=secrets.randbits(1024)
q=next_prime(q)
print("q={0:x}".format(q))

n=int(p*q)
print("n=p*q={0:x}".format(n))
print(len(bin(n))-2, bytes_needed(n))

n_encoded=n.to_bytes(bytes_needed(n), 'big')
print(hexlify(n_encoded))
print("entropy: {}".format(entropy(n_encoded)))

banner("RSA Schluessel c")
print("Test: durch einen unglücklichen Fehler werden die letzten 512 Bit (64 Byte) von n auf Null gesetzt.")

n_encoded_2=n_encoded[:-64] + bytes([ 0 for i in range(0,64) ])
print(len(n_encoded_2))
print(hexlify(n_encoded_2))
print("entropy: {}".format(entropy(n_encoded_2)))

banner("RSA Schluessel d")
print("Test: durch einen unglücklichen Fehler werden die ersten 256 Byte der base64-Kodierung von n verwendet.")
b=base64.b64encode(n_encoded)
print(len(b), str(b))
b2=b[0:256]
print(len(b2), str(b2))
print("entropy: {}".format(entropy(b2)))

banner("RSA Schluessel e")
print("Test: durch einen unglücklichen Fehler werden Bitfolgen im Schlüssel wiederholt:")
print('''
-----BEGIN PUBLIC KEY-----
MIIBIzANBgkqhkiG9w0BAQEFAAOCARAAMIIBCwKCAQIA8h
O1l3lbPR8A4sSmiGpMLg/x07WXeVs9HwDixKaIakwuD/HTtZd5Wz0fAOLEpohqTC4P8d
O1l3lbPR8A4sSmiGpMLg/x07WXeVs9HwDixKaIakwuD/HTtZd5Wz0fAOLEpohqTC4P8d
O1l3lbPR8A4sSmiGpMLg/x07WXeVs9Hxeb
        8A4sSmiGpMLg/x07WXeVs9HwDixKaIakwuD/HTtZd5Wz0fAOLEpohqTC4P8d
O1l3lbPR8A4sSmiGpMLg/x07WXeVs9HwDixKaIakwuD/HTtZd5Wz0fAOLEpohqTC4P8d
O1l3lbPR8A4sSmiGpMLg/x07WXeVs9HwDixKaIakw
ssCAwEAAQ==
-----END PUBLIC KEY-----
''')

n=0xF213B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F179BF00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2CB
n_encoded=n.to_bytes(bytes_needed(n), 'big')
print(bytes_needed(n), hexlify(n_encoded))
print("entropy: {}".format(entropy(n_encoded)))

