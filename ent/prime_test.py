#! /usr/bin/env python3

from hashlib import sha256
from binascii import hexlify
import secrets, math, sys, pyfiglet, shutil, base64
from gmpy2 import next_prime
from collections import Counter


def bytes_needed(n):
    if n == 0:
        return 1
    return int(math.log(n, 256)) + 1

def i2osp(integer, size=4):
  return ''.join([chr((integer >> (8 * i)) & 0xFF) for i in reversed(range(size))])

def banner(s):
    if not s:
        return 
    #rows, columns = os.popen('stty size', 'r').read().split()
    x=shutil.get_terminal_size((80, 24))
    print(pyfiglet.Figlet(font='cybermedium', width=int(x.columns)).renderText(s))

# experimenting with the entropy-measure-function from
# https://rosettacode.org/wiki/Entropy#Python
def entropy(s):
    p, lns = Counter(s), float(len(s))
    return -sum( count/lns * math.log(count/lns, 2) for count in p.values())

proz=[ 0.5 ]

p=2
for i in range(24):
    p=next_prime(p)
    proz.append( proz[-1] + (1-proz[-1])/float(p) )

print(len(proz), ":", proz)

B=100; i=2; primes=[ ];
while i < B:
    primes.append(i)
    i=int(next_prime(i))
print("B={}".format(B))
print(len(primes), ":", primes)

gefunden=0; Anz=10**5
for i in range(0,Anz):
    n=secrets.randbits(2048)
    found=False
    for x in primes:
        if n%x==0:
            found=True
            break
    if found:
        gefunden+=1

print(gefunden, "von", Anz, gefunden/float(Anz))

n=0xF213B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F179BF00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2E0FF1D3B597795B3D1F00E2C4A6886A4C2CB
print("ich probiere mich an", hex(n))

found=False
B=10**5; i=2; num=1
while i<B:
    if n%i==0:
        found=True
        print("{} ({}-te Primzahl)".format(i,num))
        break
    i=next_prime(i)
    num+=1

print("Gefunden:", found)

