#include <stdio.h>
#include <openssl/obj_mac.h>
#include <openssl/ec.h>
#include <openssl/rand.h>

int handleErrors() {

     printf("Ein Fehler ist aufgetreten.\n");
}

int main(void) {

     EC_KEY *key;
     unsigned char * buff;
     // Achtung: soll eine Zweierpotenz sein:
     unsigned char buff2[32];
     int mylen=0, i;
     FILE * myfile;

     const int max_elemente = 2<<19;
     const int ten_percent=max_elemente / 10;
     
     if (NULL == (
	      	key = EC_KEY_new_by_curve_name(NID_X9_62_prime256v1)
	      )
	) handleErrors();
     
     if (! (myfile=fopen("ecc-priv-keys.bin", "wb"))) handleErrors();

     printf("Ich erzeuge %d ECC-Schlüsselpaare.\n", max_elemente);
     for (i=0; i<max_elemente; i++) {
     	if (1 != EC_KEY_generate_key(key)) handleErrors();
        mylen=EC_KEY_priv2buf(key, &buff);
        // printf("mylen=%d\n", mylen);
     	fwrite(buff, mylen, 1, myfile);
	if (i % ten_percent ==0) {
		printf("i=%d (%2.2f%%)\n", i, 100.0 * (float) i/ (float) max_elemente);
	}
     }

     if (fclose(myfile)) handleErrors();

     myfile=fopen("aes-keys.bin", "wb");

     printf("Ich erzeuge %d 256-Bit-AES-Schlüssel.\n", max_elemente);
     for (i=0; i<max_elemente; i++) {
        if (!RAND_bytes(buff2, sizeof buff2)) handleErrors();
        fwrite(buff2, sizeof buff2, 1, myfile);
	if (i % ten_percent ==0) {
		printf("i=%d (%2.2f%%)\n", i, 100.0 * (float) i/ (float) max_elemente);
        }
     }

     if (fclose(myfile)) handleErrors();

     return 0;
}
     
