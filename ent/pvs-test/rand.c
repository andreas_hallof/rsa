#include <stdio.h>
#include <openssl/rand.h>

unsigned char key[16], iv[16];

int main(void) {

	if (!RAND_bytes(key, sizeof key)) {
		printf("ups1\n");    
	}

	if (!RAND_bytes(iv, sizeof iv)) {
		printf("ups2\n");    
	}

	return 0;
}
