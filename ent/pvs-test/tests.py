#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys

sys.path.append( os.path.join(os.getcwd(), os.pardir) )
sys.path.append( os.getcwd())

from nistsp800_22 import *

if __name__ == '__main__':

    print("Ich teste mit ecc-priv-keys.bin.")

    with open("ecc-priv-keys.bin", "rb") as f:
         data=f.read()

    print("Datenlänge ist {}.".format(len(data)))

    print("monobit (>0.01)", monobit(data))
    print("block frequency (>0.01)", block_frequency(data))
    print("independent runs (>0.01)", independent_runs(data))
    print("longest runs (>0.01)", longest_runs(data))
    print("matrix rank (>0.01)", matrix_rank(data))
    #x#print("spectral (>0.01)", spectral(data))
    print("non_overlapping_patterns (>0.01)", non_overlapping_patterns(data))
    print("overlapping_patterns (>0.01)", overlapping_patterns(data))
    print("universal (>0.01)", universal(data))
    print("serial (>0.01)", serial(data))
    print("approximate_entropy (>0.01)", approximate_entropy(data))
    print("cumulative_sums (>0.01)", cumulative_sums(data))
    print("random_excursions (>0.01)", random_excursions(data))
    print("random_excursions_variant (>0.01)", random_excursions_variant(data))

