# Schlüsselerzeugung mit OpenSSL

## gematik notebook (normales windows 10)

    $ ./a.exe
    Ich erzeuge 1048576 ECC-Schlüsselpaare.
    i=0 (0.00%)
    i=104857 (10.00%)
    i=209714 (20.00%)
    i=314571 (30.00%)
    i=419428 (40.00%)
    i=524285 (50.00%)
    i=629142 (60.00%)
    i=733999 (70.00%)
    i=838856 (80.00%)
    i=943713 (90.00%)
    i=1048570 (100.00%)
    Ich erzeuge 33554432 256-Bit-AES-Schlüssel.
    i=0 (0.00%)
    i=104857 (10.00%)
    i=209714 (20.00%)
    i=314571 (30.00%)
    i=419428 (40.00%)
    i=524285 (50.00%)
    i=629142 (60.00%)
    i=733999 (70.00%)
    i=838856 (80.00%)
    i=943713 (90.00%)
    i=1048570 (100.00%)

    $ ls -l *.bin
    -rwxr-xr-x 1 a users 33554432 Apr  6 10:19 aes-keys.bin
    -rw-r--r-- 1 a users 33554432 Apr  6 10:19 ecc-priv-keys.bin

    $ time ./test.py
    Ich teste mit ecc-priv-keys.bin.
    Datenlänge ist 33554432.
    monobit (>0.01) 0.8835390238098174
    block frequency (>0.01) 0.10196571633680843
    independent runs (>0.01) 0.8740071575280727
    longest runs (>0.01) 0.10181672601670202
    matrix rank (>0.01) 0.5035872441594452
    non_overlapping_patterns (>0.01) 0.6705728008177712
    overlapping_patterns (>0.01) 3.3370284333226045e-06
    universal (>0.01) 0.005135635632818983
    serial (>0.01) 0.14463875845330418
    approximate_entropy (>0.01) 0.8511868430515812
    cumulative_sums (>0.01) 0.7662250524359844
    random_excursions (>0.01) [0.17778526550659945, 0.35720592001615337, 0.24631019557432962, 0.38106562424833684, 0.7285207892511331, 0.6016856980611538, 0.2391378436473921, 0.2911018363253169]
    random_excursions_variant (>0.01) [0.11697887712373203, 0.09886049738836238, 0.08305006409959512, 0.09012312560464347, 0.09229178409788787, 0.09106388417097808, 0.22780721211576782, 0.850981499860729, 0.8690024867872712, 0.7114060382621027, 0.6085610474392021, 0.575374038600915, 0.5187572223128909, 0.7403898267322286, 0.6525441811258155, 0.5612074669871342, 0.47407454846263386, 0.3332561437497763]

    real    101m2,597s
    user    78m4,613s
    sys     0m51,162s

## alter Linux PC

    $ ./a
    Ich erzeuge 1048576 ECC-Schlüsselpaare.
    i=0 (0.00%)
    i=104857 (10.00%)
    i=209714 (20.00%)
    i=314571 (30.00%)
    i=419428 (40.00%)
    i=524285 (50.00%)
    i=629142 (60.00%)
    i=733999 (70.00%)
    i=838856 (80.00%)
    i=943713 (90.00%)
    i=1048570 (100.00%)
    Ich erzeuge 33554432 256-Bit-AES-Schlüssel.
    i=0 (0.00%)
    i=104857 (10.00%)
    i=209714 (20.00%)
    i=314571 (30.00%)
    i=419428 (40.00%)
    i=524285 (50.00%)
    i=629142 (60.00%)
    i=733999 (70.00%)
    i=838856 (80.00%)
    i=943713 (90.00%)
    i=1048570 (100.00%)

    $ ls -l *.bin
    -rw-r--r-- 1 a users 33554432  4. Apr 18:48 aes-keys.bin
    -rw-r--r-- 1 a users 33554432  4. Apr 18:47 ecc-priv-keys.bin 
 
    $ ent ecc-priv-keys.bin 
    Entropy = 7.999994 bits per byte.
    
    Optimum compression would reduce the size
    of this 33554432 byte file by 0 percent.
    
    Chi square distribution for 33554432 samples is 265.56, and randomly
    would exceed this value 31.18 percent of the times.
    
    Arithmetic mean value of data bytes is 127.4897 (127.5 = random).
    Monte Carlo value for Pi is 3.141394087 (error 0.01 percent).
    Serial correlation coefficient is 0.000015 (totally uncorrelated = 0.0).
 
    $ ent aes-keys.bin 
    Entropy = 7.999995 bits per byte.
    
    Optimum compression would reduce the size
    of this 33554432 byte file by 0 percent.
    
    Chi square distribution for 33554432 samples is 247.80, and randomly
    would exceed this value 61.49 percent of the times.
    
    Arithmetic mean value of data bytes is 127.4976 (127.5 = random).
    Monte Carlo value for Pi is 3.141907641 (error 0.01 percent).
    Serial correlation coefficient is 0.000028 (totally uncorrelated = 0.0).
 
 
    $ ./test.py
    Ich teste mit ecc-priv-keys.bin.
    Datenlänge ist 33554432.
    monobit (>0.01) 0.20521046657117403
    block frequency (>0.01) 0.6370922534736004
    independent runs (>0.01) 0.1579607976199472
    longest runs (>0.01) 0.8205972670314339
    matrix rank (>0.01) 0.4505559718448671
    non_overlapping_patterns (>0.01) 0.5262035706541462
    overlapping_patterns (>0.01) 1.2965608459974496e-06
    universal (>0.01) 0.8246993638474005
    serial (>0.01) 0.8204039169230196
    approximate_entropy (>0.01) 0.901720507504324
    cumulative_sums (>0.01) 0.2350162877931026
  
## Virtuelle Maschine
  
    a@t:~/git/rsa/ent/pvs-test$ ent ./ecc-priv-keys.bin 
    Entropy = 7.999994 bits per byte.
    
    Optimum compression would reduce the size
    of this 33554432 byte file by 0 percent.
    
    Chi square distribution for 33554432 samples is 265.01, and randomly
    would exceed this value 32.02 percent of the times.
    
    Arithmetic mean value of data bytes is 127.5018 (127.5 = random).
    Monte Carlo value for Pi is 3.141661593 (error 0.00 percent).
    Serial correlation coefficient is -0.000211 (totally uncorrelated = 0.0).
    
    
    a@t:~/git/rsa/ent/pvs-test$ ent ./aes-keys.bin 
    Entropy = 7.999995 bits per byte.
    
    Optimum compression would reduce the size
    of this 33554432 byte file by 0 percent.
    
    Chi square distribution for 33554432 samples is 237.80, and randomly
    would exceed this value 77.33 percent of the times.
    
    Arithmetic mean value of data bytes is 127.4934 (127.5 = random).
    Monte Carlo value for Pi is 3.142268845 (error 0.02 percent).
    Serial correlation coefficient is 0.000090 (totally uncorrelated = 0.0).
    

    a@t:$ ./test.py
    Ich teste mit ecc-priv-keys.bin.
    Datenlänge ist 33554432.
    monobit (>0.01) 0.488467068076
    block frequency (>0.01) 0.988351109851
    independent runs (>0.01) 0.379370038741
    longest runs (>0.01) 0.494930545055
    matrix rank (>0.01) 0.22189155609533734
    non_overlapping_patterns (>0.01) 0.792122691552
    overlapping_patterns (>0.01) 4.62028838133e-06
    universal (>0.01) 0.823919938584
    serial (>0.01) 0.265835399747
    approximate_entropy (>0.01) 0.314754482161
    
