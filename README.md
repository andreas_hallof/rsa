# rsa

some low-level experiments with rsa

Die Experimente bez. Kodierungsfehler-Erkennen und NIST-SP-800-22-Tests sind im [Unterverzeichnis keygen](keygen/).

## Encryption

    $ ./oaep.py
    2048 n= 0xe221fad69b5dea6676efd065f5d10896abc32370b8f80c67e92befcbbf51554a9b6cc1f14162f76a1a47f46a19862cc33d277f14dcd55478ca1b78e6c5727ab3d7a0e8db61e85817f1c46e2251822845c7b149a2576d356aa810be9f5877c4cac8e8da57490add28a797fa95ab554f28d285a432df102e0577f1197dbe29b1810400668b163e040a6926747d6fc7b4bdcbb68da81b980520e42d47c80297608eb16c00f82190eae392762175aa25377c3d67eb39e3cc9763170f86754e07de0338ddb2c6194edd5941ebc2b974dfc773f2a2f1b7e4cfb31afff6feb9bf72037d4527737e78a9d5d0629d51cb5e66ca790e8e0c70d41a7868b2154cfc3aca5149
    32 e= 0xe85dc95d
    10 message= b'Hallo Test'
    256 EM= b'\x00\xf6\xe3\x14\x186\x99\xde\xfc\xbd[\x15\x9e\xb7\n|\xd2\xd3\x95\xc3%\xa1\xde\xdd\r\x8dzo\xf7\x94\xff&\x16\x8e\x06\x9b\x97\x07/J\xe2\xe8j\xb4\x9f\x824tO\x94\x16uv\xc5\xf5\x88\xbc\x07\x1d\xddY\x84n2[!X\xa8\x90m^,+\xe0\x01\xba\xc9C\xab\x9c\xab@cSn\x1cTk@"\x1f\xdf\x8d\xb01\xa4\xbb\xe1_7D#c7\x01\xe0O\xe1|\x1dd\x0b4\xf2\xe2{\x8fj\xec\x00\xe2O\x1d\xcfP\xad\t \xb3\xb4\xe3\xc4\xfa\r\x93\xfd\x8a\xdb\x0e\x88\xd7\x1fF\x14\xd0\x18\x14\x9aY\xf6\xa5\xad\xbf\xba\x1b\xd0\x00\xd0\xe7\xc4\x9a\xd5\x84\xce\x97\xee\xce`\x8f\xe0\xed\x7fI1b\xa4\xa9\xcb\xfc\xcd\x93]\xa0\xf8\xc3\x81`\x90f\x15\x97\xac\xebUJg\xca\x9fR\xa7q\xd2ky\xbb\x0bX\x88,\x94\xfb7\xda_\x1c\xce\xac\xbc\xedCr\x16\x85#\xf4\xa8H+\xa3\x88zJ\xba\x03\xda\xd0D\x08Q\xe9\xde\x87\xb8\x03{\x7f\xc5VT\xcc<!\xa5\x130\xbe'
    c= 0xc970ab62f67cf04b40ebfd311fafa916cb2105bc195fe853880f050905ea95ad053f2a4f8859e2caa97e3a6bf3eaed123a965027de973c64df7915226aa08bee4475477d618d0f192100f01dbe39c12c2f68174fb371c19b0c04ea57c8526f5257ff29b4083f2180f06dd04bc9175234e6986b2fbca12dcf24580767d0acbda0fee3cbc84c721a6209d4d1f8b0b3f9d40cbc9d9c6636ffb88965c0b2859677995bca32dd2e93dab5f4cd87541a365eba075c7a77c874113d64caaf1b79df4f9d3cf4de73542a451f551a87013a62302d2fe8f874fccb1150208e6c3ec861aa49c386507abdb47335271f1b6ba1d871919ca692ab8bd1ef11d1e4244ec9be00ec
    c970ab62f67cf04b40ebfd311fafa916cb2105bc195fe853880f050905ea95ad053f2a4f8859e2caa97e3a6bf3eaed123a965027de973c64df7915226aa08bee4475477d618d0f192100f01dbe39c12c2f68174fb371c19b0c04ea57c8526f5257ff29b4083f2180f06dd04bc9175234e6986b2fbca12dcf24580767d0acbda0fee3cbc84c721a6209d4d1f8b0b3f9d40cbc9d9c6636ffb88965c0b2859677995bca32dd2e93dab5f4cd87541a365eba075c7a77c874113d64caaf1b79df4f9d3cf4de73542a451f551a87013a62302d2fe8f874fccb1150208e6c3ec861aa49c386507abdb47335271f1b6ba1d871919ca692ab8bd1ef11d1e4244ec9be00ec

## Tools

You can use x509der-to-n-and-e.py to extract (n,e) from a certificate.

## Encryption and Decryption

    $ ./oaep+decrypt.py
    ____ ____ ____    ____ ____ _  _ _    _  _ ____ ____ ____ ____ _
    |__/ [__  |__|    [__  |    |__| |    |  | |___ [__  [__  |___ |
    |  \ ___] |  |    ___] |___ |  | |___ |__| |___ ___] ___] |___ |___


    2048 n= 0xa1ee52b0c69cb7a0cb79ba879ca1451c35dfc469599d1cf48cbf91c5c6cf7abe6eaeffb79a4fba6969ecc55b1e4152774ab251f89c9bcb229595292624dbac37df6e534b5841712fd400562063f807841e16f0e3a2a2b66e7a7aeb7c2240fec14e2371d3b1c8540249745345004f01e991ff84588b88521f42bcb555aff87e72b54727866bb6172eccae6833a341ac0cd7f426b652686f6d15b167a2dadee7ed09044dc6317e79aac23070eda26dbf109ea809a25754679fc9847027d175d655e8b5d99dc8721e43291aa436f6a66bde15668f828e9a704fc2274c7ee8ba9eedaaa4f57485dae0379dac9e0a823302f95dfe0900c460a0fd9799ceb3c8503b89
    17 e= 0x10001
    ____ ____ ____    ____ ____ ____ ___     _  _ ____ ____ ____ ____ _  _ _    _  _ ____ ____ ____ ____ _    _  _ _  _ ____
    |__/ [__  |__| __ |  | |__| |___ |__]    |  | |___ |__/ [__  |    |__| |    |  | |___ [__  [__  |___ |    |  | |\ | | __
    |  \ ___] |  |    |__| |  | |___ |        \/  |___ |  \ ___] |___ |  | |___ |__| |___ ___] ___] |___ |___ |__| | \| |__]


    10 message= b'Hallo Test'
    32 seed= b'\xbd\xd1\x90\xdb\x1a\xc8\x98FqWfKi\xdetQ\xab\xcd\xb1\xad\x92Su\xda\xa0\x17\xb8\x8c\x1f\xb9\xa4\x99'
    256 EM= b"\x00\xba\x0e\xf9\xd2%\x0b\xd3\xd2\xb7\xe8W\xaey\xb2;\x84AoN\xb91\xd2\xb1\xc3\x0b\xfbw\x9a\xb0\xf4\x0b\xb1\xe7JHdtQ\xe4\x98Q\xd6<\x7f'r\xd9E\x93\xed\tj,,4]\x8a-\xa3\x85\x97\xbab\xa8\xc1s\x9dQ\xfd?\x9c\xf4i\x84[\x00\xc4\xcco{\x00\xc0\x9e\x8b\xd6\xd3`\xc4QA\xc8\x1c\xdcn\x8e\xf9\xaf8\x82\xd0\xf2.\x93\xc9\xf5\xe8\xd5\x92\x05\\k\x88|\\\xdfE\xbc\x10%U*&\xafv\xabR\xefYk\xd0\xc4\xe0\xa7d1z\xaaLE\xda\x9f\xc5\xf5\x7fqeG\xfe\xae\x99\xedk\x0b\t\x13M\x06\xb4\x06\xcf\xb5\x853w\xa9+Zh!\xacK!_\xd2\xfd\xd15:uD\xb2oz5\x8a\xb6\xc0F\xc8\xd8\xe0\x02\x85,Yg=V\xff_\xa8\xb1<r\xa9~\xcf\xf4\xfc\xd3\t\x91\x00Q\xf8\xf4\xdd\xca\x03|\xb1@P%\x12\xc0\x92\xf4}\x84\xba4\t\xcc\xa6\xd7^\x9f\\\xccfy\x8e\xb3\x9b\xc6\xd3\xf4T\xf69u\xeau["
    Chiffrat= b'65ea920207649ffa2e053d63c26152614120f94ccb24886f6b9fc07ec69a2579ea4f52a9e261ac0b65bb5981089a4f045c98a5a6785e7ed32972bd5665ee7656728fbe68ae7f75478bd67bacb118fd3973a3c291f6989c50fe1cffbe465698c7337f9a06b5aeb91e76e6103212a0d5bf13d66ae6b179802a81d35d4110d550dc269ef8fe2b81d0c8d447c0d1377fc09af8daa00731ea139d2a26cb69a680ac2113c2046a2f0eb50afa72929c0a0e0e0a07a9bce9afd92764ed0c195f2e6e033171a82ff09ab7187a9f2b6c75084c461ec03225152c41224925a27fcff4c68678f54eec171d4eb815566454b90c49e55fcff358d14a9dedca5612d415e9e69309'
    ____ ____ ____    ____ ____ ____ ___     ____ _  _ ___ ____ ____ _  _ _    _  _ ____ ____ ____ ____ _    _  _ _  _ ____
    |__/ [__  |__| __ |  | |__| |___ |__]    |___ |\ |  |  [__  |    |__| |    |  | |___ [__  [__  |___ |    |  | |\ | | __
    |  \ ___] |  |    |__| |  | |___ |       |___ | \|  |  ___] |___ |  | |___ |__| |___ ___] ___] |___ |___ |__| | \| |__]


    Chiffrat= b'65ea920207649ffa2e053d63c26152614120f94ccb24886f6b9fc07ec69a2579ea4f52a9e261ac0b65bb5981089a4f045c98a5a6785e7ed32972bd5665ee7656728fbe68ae7f75478bd67bacb118fd3973a3c291f6989c50fe1cffbe465698c7337f9a06b5aeb91e76e6103212a0d5bf13d66ae6b179802a81d35d4110d550dc269ef8fe2b81d0c8d447c0d1377fc09af8daa00731ea139d2a26cb69a680ac2113c2046a2f0eb50afa72929c0a0e0e0a07a9bce9afd92764ed0c195f2e6e033171a82ff09ab7187a9f2b6c75084c461ec03225152c41224925a27fcff4c68678f54eec171d4eb815566454b90c49e55fcff358d14a9dedca5612d415e9e69309'
    c= 0x65ea920207649ffa2e053d63c26152614120f94ccb24886f6b9fc07ec69a2579ea4f52a9e261ac0b65bb5981089a4f045c98a5a6785e7ed32972bd5665ee7656728fbe68ae7f75478bd67bacb118fd3973a3c291f6989c50fe1cffbe465698c7337f9a06b5aeb91e76e6103212a0d5bf13d66ae6b179802a81d35d4110d550dc269ef8fe2b81d0c8d447c0d1377fc09af8daa00731ea139d2a26cb69a680ac2113c2046a2f0eb50afa72929c0a0e0e0a07a9bce9afd92764ed0c195f2e6e033171a82ff09ab7187a9f2b6c75084c461ec03225152c41224925a27fcff4c68678f54eec171d4eb815566454b90c49e55fcff358d14a9dedca5612d415e9e69309
    223 DB= bytearray(b"\xe3\xb0\xc4B\x98\xfc\x1c\x14\x9a\xfb\xf4\xc8\x99o\xb9$\'\xaeA\xe4d\x9b\x93L\xa4\x95\x99\x1bxR\xb8U\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01Hallo Test")
    message= bytearray(b'Hallo Test')


