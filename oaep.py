#! /usr/bin/env python3

from hashlib import sha256
from math import log


def bytes_needed(n):
    if n == 0:
        return 1
    return int(log(n, 256)) + 1

def i2osp(integer, size=4):
  return ''.join([chr((integer >> (8 * i)) & 0xFF) for i in reversed(range(size))])

def mgf1(mgfSeed, length, hash=sha256):
  counter = 0
  output = b''
  while (len(output) < length):
    C = i2osp(counter, 4); C = C.encode()
    output += hash(mgfSeed + C).digest()
    counter += 1
  return output[:length]


n=0xe221fad69b5dea6676efd065f5d10896abc32370b8f80c67e92befcbbf51554a9b6cc1f14162f76a1a47f46a19862cc33d277f14dcd55478ca1b78e6c5727ab3d7a0e8db61e85817f1c46e2251822845c7b149a2576d356aa810be9f5877c4cac8e8da57490add28a797fa95ab554f28d285a432df102e0577f1197dbe29b1810400668b163e040a6926747d6fc7b4bdcbb68da81b980520e42d47c80297608eb16c00f82190eae392762175aa25377c3d67eb39e3cc9763170f86754e07de0338ddb2c6194edd5941ebc2b974dfc773f2a2f1b7e4cfb31afff6feb9bf72037d4527737e78a9d5d0629d51cb5e66ca790e8e0c70d41a7868b2154cfc3aca5149
e=0xe85dc95d

print(len(bin(n))-2, "n=",hex(n))
print(len(bin(e))-2, "e=",hex(e))

message="Hallo Test".encode()

print(len(message), "message=", message)

IHash=sha256(b'').digest()
Plen=bytes_needed(n)-len(message)-66
p=0
PS=p.to_bytes(Plen, 'big')
DB=IHash + PS + b'\x01' + message 
# if you want to use random seeds cf. oaep+decrypt.py
seed=bytes(32)  # seed ist damit \x00\x00...
dbMask=mgf1(seed, bytes_needed(n)-32-1)
maskedDB=bytearray([ a ^ b for (a, b) in zip(DB, dbMask) ])
seedMask=mgf1(maskedDB, len(IHash))
maskedSeed=bytearray([ a ^ b for (a, b) in zip(seed, seedMask) ])
EM=b'\x00' + maskedSeed + maskedDB

print(len(EM), "EM=", EM)
m=int.from_bytes(EM, byteorder='big', signed=False)
assert m<n

c=pow(m, e, n)

print("c=",hex(c))

C=c.to_bytes(bytes_needed(c), byteorder='big')

print("Chiffrat=", "".join("{:02x}".format(x) for x in C))


