#! /usr/bin/env python3

import os
from time import time
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.asymmetric import rsa, padding

private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend()
    )

public_key=private_key.public_key()

message = "".join([" " for i in range(190)]).encode("ascii")

print(len(message))

start=time()
num_iter=100000
for i in range(num_iter):
    #message = b"encrypted data 0123456789ABCDEF"
    ciphertext = public_key.encrypt(
        message,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )

print("Anzahl bytes", 190*num_iter/1024.0/1024.0, "Zeit", time()-start)

backend = default_backend()
key = os.urandom(32)
iv = os.urandom(16)
cipher = Cipher(algorithms.AES(key), modes.CBC(iv), backend=backend)
encryptor = cipher.encryptor()

#ct = encryptor.update(b"a secret message") + encryptor.finalize()

num_iter2=1187500 # num_iter*190/16

start=time()
for i in range(num_iter2):
    ct = encryptor.update(b"a secret message")

#decryptor = cipher.decryptor()
#decryptor.update(ct) + decryptor.finalize()

print("Anzahl bytes", 16*num_iter2/1024.0/1024.0, "Zeit", time()-start)



