#! /usr/bin/env python3

# https://github.com/mhuisman/pi-spigot-py2k/blob/master/calc_pi.py
def basic_iterative(digits):
    pi = 0
    p16 = 1
    for k in range(0, digits):
        pi += 1 / p16 * (4 / (8 * k + 1) - 2 / (8 * k + 4) - 1 / (8 * k + 5) - 1 / (8 * k + 6))
        p16 *= 16

    return pi

print(basic_iterative(100))

