#! /usr/bin/env python3

from hashlib import sha256
from binascii import hexlify
import secrets, math, sys, pyfiglet, shutil


def bytes_needed(n):
    if n == 0:
        return 1
    return int(math.log(n, 256)) + 1

def i2osp(integer, size=4):
  return ''.join([chr((integer >> (8 * i)) & 0xFF) for i in reversed(range(size))])

def mgf1(mgfSeed, length, hash=sha256):
  counter = 0
  output = b''
  while (len(output) < length):
    C = i2osp(counter, 4); C = C.encode()
    output += hash(mgfSeed + C).digest()
    counter += 1
  return output[:length]

def banner(s):
    if not s:
        return 
    #rows, columns = os.popen('stty size', 'r').read().split()
    x=shutil.get_terminal_size((80, 24))
    print(pyfiglet.Figlet(font='cybermedium', width=int(x.columns)).renderText(s))

banner("RSA Schluessel")
n=0x00A1EE52B0C69CB7A0CB79BA879CA1451C35DFC469599D1CF48CBF91C5C6CF7ABE6EAEFFB79A4FBA6969ECC55B1E4152774AB251F89C9BCB229595292624DBAC37DF6E534B5841712FD400562063F807841E16F0E3A2A2B66E7A7AEB7C2240FEC14E2371D3B1C8540249745345004F01E991FF84588B88521F42BCB555AFF87E72B54727866BB6172ECCAE6833A341AC0CD7F426B652686F6D15B167A2DADEE7ED09044DC6317E79AAC23070EDA26DBF109EA809A25754679FC9847027D175D655E8B5D99DC8721E43291AA436F6A66BDE15668F828E9A704FC2274C7EE8BA9EEDAAA4F57485DAE0379DAC9E0A823302F95DFE0900C460A0FD9799CEB3C8503B89
e=65537
d=0x6A930D0F55987D6EB19C8B3C87C4C85479CCAD5B5CE47FD31FF35140A6C99512528C90B6C933E20F50ECC933602543DE49663576F7B3BDAD245FC8765759D0EDF0FA8E8D8F277B142C669048BF22D5E7895A35EECBF265392F19497528A23849076162A01E6927895DC8D6A91A3BE5995142A75B13728402190D5417723A8D156BE2379E6823068976F8C8424D5A1D237D6043C87CB61298A3C08A034D6EC2027A39DF73D3EB01C17DAB15A497F9D33D6CBFF7DCE40D324966CACE730E558708F880801760F7D3B24B13C66827B2F927E75E031C6532B7166B690B92EAC549FD78BDA40FFFFDB0A5ECEB3A99AA2C6A422568FCBBA1775C5637A0233514F88401

test_x=10**10-1
assert pow( pow( test_x, e, n), d, n) == test_x

print(len(bin(n))-2, "n=",hex(n))
print(len(bin(e))-2, "e=",hex(e))

banner("RSA-OAEP Verschluesselung")
message="Hallo Test".encode()

print(len(message), "message=", message)

lHash=sha256(b'').digest()
Plen=bytes_needed(n)-len(message)-66
p=0
PS=p.to_bytes(Plen, 'big')
DB=lHash + PS + b'\x01' + message 

tmp=secrets.randbits(256)
seed=tmp.to_bytes(32, byteorder='big')
print(len(seed), "seed=", seed)

dbMask=mgf1(seed, bytes_needed(n)-32-1)
maskedDB=bytearray([ a ^ b for (a, b) in zip(DB, dbMask) ])
seedMask=mgf1(maskedDB, len(lHash))
maskedSeed=bytearray([ a ^ b for (a, b) in zip(seed, seedMask) ])
EM=b'\x00' + maskedSeed + maskedDB

print(len(EM), "EM=", EM)
m=int.from_bytes(EM, byteorder='big', signed=False)
assert m<n

c=pow(m, e, n); # print("c=",hex(c))

C=c.to_bytes(bytes_needed(c), byteorder='big')

print("Chiffrat=", hexlify(C))

banner("RSA-OAEP Entschluesselung"); assert len(C)<=bytes_needed(n); print("Chiffrat=", hexlify(C))

c=int.from_bytes(C, byteorder='big', signed=False); print("c=", hex(c))
m=pow(c,d,n)
EM=m.to_bytes(bytes_needed(n), byteorder='big')
Y=EM[0]; maskedSeed=EM[1:1+32]; maskedDB=EM[32+1:]
seedMask = mgf1(maskedDB, 32)
seed=bytearray([ a ^ b for (a, b) in zip(maskedSeed, seedMask) ])
dbMask = mgf1(seed, bytes_needed(n) - 32 - 1)
DB=bytearray([ a ^ b for (a, b) in zip(maskedDB, dbMask) ]); print(len(DB), "DB=", DB)

lHash=sha256(b'').digest()
if not DB[:32]==lHash:
    print("Decryption Error")
    sys.exit(1)

message=DB[32:]

offset=0
for x in message:
    if x==1:
        break
    if x!=0:
        print("Decryption Error")
        sys.exit(1)
    offset+=1

message=message[offset+1:]
print("message=", message)

