#! /usr/bin/env python3

from hashlib import sha256
from binascii import hexlify
import math, pyfiglet
from KeyGenUtils import *

db=PrimeDB("primes.db"); print(db)
Anz=db.getNumberOfElements()
#Anz=1000
found=0; 
t=SmallPrimeFactorTest(Total=(Anz**2)-Anz)
print(t, t.PrimesStr(), "Ich teste {} Paare.".format(Anz**2-Anz))
print("Memory:", MemUsage())

n=db.get(0)*db.get(1)
print(hex(n))
n_encoded_1=n.to_bytes(bytes_needed(n), 'little')
n_2=int.from_bytes(n_encoded_1, byteorder='big', signed=False)
print(hex(n_2))

widgets = [progressbar.Percentage(), ' ', progressbar.ETA(), ' ', progressbar.Bar()]
bar = progressbar.ProgressBar(widgets=widgets, max_value=(Anz)**2).start()
for a in range(0, Anz):
    for b in range(0, Anz):
        if a!=b:
            n=db.get(a)*db.get(b)
            n_encoded_1=n.to_bytes(bytes_needed(n), 'little')
            n_wrong_byteorder=int.from_bytes(n_encoded_1, byteorder='big', signed=False)
            if t.test(n_wrong_byteorder):
                found+=1

        bar.update(a*Anz + b)
bar.finish()

print("{} von {} ({:3.6f}%)".format(found, Anz**2-Anz, 100*found/float(Anz**2-Anz) ))

print(t.stats())
print(t.MissingPrimesStr())


