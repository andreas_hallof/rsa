#! /usr/bin/env python3

from math import log10

n=10**6
while n**2-n<10**12:
    n+=1

t=n**2-n
ist=9999900000

print(n, t, log10(t), ist, log10(ist))

n=1000
t=n**2-n

x=15*60*(10**12-10**6)/(t)
print(x)

