# Test Linux PC

    e3df25d49a376544ff4ed3270f40b499fe36d935e460ecfcecd7a9ee1cf8bc78  primes.2.db

    ...
    Bin bei 9995 von 10000 (99.950000%) (min=6.857646).
    Bin bei 9996 von 10000 (99.960000%) (min=6.857646).
    Bin bei 9997 von 10000 (99.970000%) (min=6.857646).
    Bin bei 9998 von 10000 (99.980000%) (min=6.857646).
    Bin bei 9999 von 10000 (99.990000%) (min=6.857646).
    Fertig, min = 6.857646.
    Benötigte Zeit: 607.791936000 Sekunden, Zeit (Sekunden) pro Element: 0.000006079
    bye


    67eccc546a1ee857da45546f79bff8d845173a3ae42093773b63596067c25885  primes.1.db

    Bin bei 9995 von 10000 (99.950000%) (min=6.864135).
    Bin bei 9996 von 10000 (99.960000%) (min=6.864135).
    Bin bei 9997 von 10000 (99.970000%) (min=6.864135).
    Bin bei 9998 von 10000 (99.980000%) (min=6.864135).
    Bin bei 9999 von 10000 (99.990000%) (min=6.864135).
    Fertig, min = 6.864135.
    Benötigte Zeit: 607.128931000 Sekunden, Zeit (Sekunden) pro Element: 0.000006072
    bye


    $ ./GenPrimesDB.py 
    Memory: 21008
    Ich erzeuge 10000 1024-Bit-Primzahlen.
    Plattform: Linux (native byteorder: little)
    100% Time:  0:12:21 |#########################|
    $ 

    
    3b80b0271690709b3e405acdfb8fcb6db93ec720461ef9e67d25e074c384dfe7  primes.db

    Bin bei 9994 von 10000 (99.940000%) (min=6.821633).
    Bin bei 9995 von 10000 (99.950000%) (min=6.821633).
    Bin bei 9996 von 10000 (99.960000%) (min=6.821633).
    Bin bei 9997 von 10000 (99.970000%) (min=6.821633).
    Bin bei 9998 von 10000 (99.980000%) (min=6.821633).
    Bin bei 9999 von 10000 (99.990000%) (min=6.821633).
    Fertig, min = 6.821633.
    Benötigte Zeit: 607.669365000 Sekunden, Zeit (Sekunden) pro Element: 0.000006077
    bye

# virtuelle Linux-Instanz Frankfurt


    8af17787de3c4d67fe9d02ae36a424fd7a71df4d25277824100febf29a72e291  primes.db

    Bin bei 9991 von 10000 (99.910000%) (min=6.849290).
    Bin bei 9992 von 10000 (99.920000%) (min=6.849290).
    Bin bei 9993 von 10000 (99.930000%) (min=6.849290).
    Bin bei 9994 von 10000 (99.940000%) (min=6.849290).
    Bin bei 9995 von 10000 (99.950000%) (min=6.849290).
    Bin bei 9996 von 10000 (99.960000%) (min=6.849290).
    Bin bei 9997 von 10000 (99.970000%) (min=6.849290).
    Bin bei 9998 von 10000 (99.980000%) (min=6.849290).
    Bin bei 9999 von 10000 (99.990000%) (min=6.849290).
    Fertig, min = 6.849290.
    Benötigte Zeit: 387.616186000 Sekunden, Zeit (Sekunden) pro Element: 0.000003877
    bye
   

    Bin bei 9996 von 10000 (99.960000%) (min=6.849290).
    Bin bei 9997 von 10000 (99.970000%) (min=6.849290).
    Bin bei 9998 von 10000 (99.980000%) (min=6.849290).
    Bin bei 9999 von 10000 (99.990000%) (min=6.849290).
    Fertig, min = 6.849290.
    Benötigte Zeit: 387.220084000 Sekunden, Zeit (Sekunden) pro Element: 0.000003873
    Ich habe 99990000 RSA-Moduli untersucht.
    bye

    a@t:~/git/rsa/keygen$ ./aa.py
    Memory: 15112
    Ich erzeuge 100000 1024-Bit-Primzahlen.
    Plattform: Linux (native byteorder: little)
    100% Time:  1:13:23 |################################|

    
    603c0d3831508cf7b07130424da3b3dcf6b7ae9a63f2ab2e874017f9160ffd86  primes.db
    
    Bin bei 99992 von 100000 (99.992000%) (min=6.799957).
    Bin bei 99993 von 100000 (99.993000%) (min=6.799957).
    Bin bei 99994 von 100000 (99.994000%) (min=6.799957).
    Bin bei 99995 von 100000 (99.995000%) (min=6.799957).
    Bin bei 99996 von 100000 (99.996000%) (min=6.799957).
    Bin bei 99997 von 100000 (99.997000%) (min=6.799957).
    Bin bei 99998 von 100000 (99.998000%) (min=6.799957).
    Bin bei 99999 von 100000 (99.999000%) (min=6.799957).
    Fertig, min = 6.799957.
    Benötigte Zeit: 42573.805869000 Sekunden, Zeit (Sekunden) pro Element: 0.000004257
    Ich habe 9999900000 RSA-Moduli untersucht.
    bye

# Vergleich python und C

    $ ls -l primes.db
    -rw-rw-r--+ 1 125K 28. Dez 22:12 primes.db

    $ sha256sum.exe primes.db 
    500a5716cac641686e30a54da7d548e3a7bd516263673ba773a19ea51192114e *primes.db

    $ ./rsakey-ent.exe 
    1.846439
    1.846439
    Nr of primes in DB: 1000
    Ich teste 999000 Moduli.
    Los geht es.
    new min 7.159774
    new min 7.152996
    new min 7.150047
    new min 7.147742
    new min 7.147352
    new min 7.121847
    new min 7.109561
    new min 7.103663
    new min 7.054737
    new min 7.049466
    new min 7.022979
    new min 7.010003
    new min 7.007608
    new min 6.982457
    new min 6.950853
    new min 6.927669
    new min 6.917524
    Fertig, min = 6.917524.
    Benötigte Zeit: 4.555000000 Sekunden, Zeit (Sekunden) pro Element: 0.000004560
    Ich habe 999000 RSA-Moduli untersucht.
    bye

    real	0m4,633s
    user	0m4,601s
    sys	0m0,000s


    $ time ./rsakey-ent.py 
    Ich habe primes.db (size=128000), damit Elemente=1000.
    Ich teste 999000 Moduli (5.999565488225983).

    new minimal entropy 7.152995751809458
    new minimal entropy 7.150046972504132
    new minimal entropy 7.147741931622168
    new minimal entropy 7.14735183212955
    new minimal entropy 7.121846656297595
    new minimal entropy 7.109560535095537
    new minimal entropy 7.103662976484885
    new minimal entropy 7.054736574533665
    new minimal entropy 7.049466480465638
    new minimal entropy 7.022979296672176
    new minimal entropy 7.010003162368886
    new minimal entropy 7.007607935602921
    new minimal entropy 6.982457303189846
    new minimal entropy 6.950852759770965
    new minimal entropy 6.927668898701709
    new minimal entropy 6.917524365334873



    real	1m18,780s
    user	1m18,467s
    sys	0m0,187s


merker https://vi.stackexchange.com/questions/3555/delete-lines-that-match-a-pattern-from-a-given-line-to-the-end-of-the-file

# Snapshot 10^12 Schlüssel-Test

	Bin bei 142831 von 1000001 (14.283086%) (min=6.775249).
	ETA 40.796967d 979.127200h, 58747.632007m, 3524857.920396s
	HIST 15 7.485750 3
	HIST 16 7.455500 481
	HIST 17 7.425250 20450
	HIST 18 7.395000 571870
	HIST 19 7.364750 9365312
	HIST 20 7.334500 99828428
	HIST 21 7.304250 678637999
	HIST 22 7.274000 3089503026
	HIST 23 7.243750 9477583035
	HIST 24 7.213500 20095486820
	HIST 25 7.183250 29747362666
	HIST 26 7.153000 31849919003
	HIST 27 7.122750 25035586370
	HIST 28 7.092500 14137878259
	HIST 29 7.062250 6064840388
	HIST 30 7.032000 1949335550
	HIST 31 7.001750 486025097
	HIST 32 6.971500 94014648
	HIST 33 6.941250 14164862
	HIST 34 6.911000 1699622
	HIST 35 6.880750 162358
	HIST 36 6.850500 12902
	HIST 37 6.820250 809
	HIST 38 6.790000 40
	HIST 39 6.759750 2
	HISTERR 0

	Bin bei 210570 von 1000001 (21.056979%) (min=6.775249).
	ETA 37.522203d 900.532874h, 54031.972425m, 3241918.345514s
	HIST 15 7.485750 4
	HIST 16 7.455500 708
	HIST 17 7.425250 30281
	HIST 18 7.395000 842475
	HIST 19 7.364750 13807779
	HIST 20 7.334500 147177128
	HIST 21 7.304250 1000491195
	HIST 22 7.274000 4554802742
	HIST 23 7.243750 13972331029
	HIST 24 7.213500 29625620050
	HIST 25 7.183250 43854972408
	HIST 26 7.153000 46955049250
	HIST 27 7.122750 36909066433
	HIST 28 7.092500 20842974117
	HIST 29 7.062250 8941165551
	HIST 30 7.032000 2873872928
	HIST 31 7.001750 716530104
	HIST 32 6.971500 138611169
	HIST 33 6.941250 20887641
	HIST 34 6.911000 2507436
	HIST 35 6.880750 239444
	HIST 36 6.850500 18879
	HIST 37 6.820250 1184
	HIST 38 6.790000 62
	HIST 39 6.759750 3
	HISTERR 0

