#! /usr/bin/env python3

import math, pyfiglet, base64, progressbar
from KeyGenUtils import *

db=PrimeDB("primes.db"); print(db)
Anz=db.getNumberOfElements()
#Anz=1000
found1=found2=0; 
t1=SmallPrimeFactorTest(Total=(Anz**2)-Anz)
t2=SmallPrimeFactorTest(Total=(Anz**2)-Anz)
print(t1)
print(t1.PrimesStr())
print("Ich teste {} Paare.".format(Anz**2-Anz))


widgets = [progressbar.Percentage(), ' ', progressbar.ETA(), ' ', progressbar.Bar()]
bar = progressbar.ProgressBar(widgets=widgets, max_value=(Anz)**2).start()
for a in range(0, Anz):
    for b in range(0, Anz):
        if a!=b:
            n=db.get(a)*db.get(b)
            n_encoded_1=n.to_bytes(bytes_needed(n), 'big')
            n_encoded_2=base64.b64encode(n_encoded_1)
            n_encoded_3=n_encoded_2[:256]
            n_wrong1=int.from_bytes(n_encoded_2, byteorder='big', signed=False)
            n_wrong2=int.from_bytes(n_encoded_3, byteorder='big', signed=False)
            if t1.test(n_wrong1):
                found1+=1
            if t2.test(n_wrong2):
                found2+=1

        bar.update(a*Anz + b)
bar.finish()

print("Ohne Cut {} von {} ({:3.6f}%)".format(found1, Anz**2-Anz, 100*found1/float(Anz**2-Anz) ))
print("Mit  Cut {} von {} ({:3.6f}%)".format(found2, Anz**2-Anz, 100*found2/float(Anz**2-Anz) ))

print(t1.stats())
print(t1.MissingPrimesStr())
print(t2.stats())
print(t2.MissingPrimesStr())



