# rsakey-ent-hist
	
	603c0d3831508cf7b07130424da3b3dcf6b7ae9a63f2ab2e874017f9160ffd86  primes.db

	Bin bei 99999 von 100000 (99.999000%) (min=6.799957).
	HIST 16 7.455500 40
	HIST 17 7.425250 1374
	HIST 18 7.395000 40040
	HIST 19 7.364750 656112
	HIST 20 7.334500 6994000
	HIST 21 7.304250 47506742
	HIST 22 7.274000 216315334
	HIST 23 7.243750 663534232
	HIST 24 7.213500 1406856808
	HIST 25 7.183250 2082562606
	HIST 26 7.153000 2229859664
	HIST 27 7.122750 1752823832
	HIST 28 7.092500 989846268
	HIST 29 7.062250 424649174
	HIST 30 7.032000 136509898
	HIST 31 7.001750 34030698
	HIST 32 6.971500 6587834
	HIST 33 6.941250 993696
	HIST 34 6.911000 119314
	HIST 35 6.880750 11458
	HIST 36 6.850500 832
	HIST 37 6.820250 40
	HIST 38 6.790000 4
	HISTERR 0
	Fertig, min = 6.799957.
	Benötigte Zeit: 40157.155869000 Sekunden, Zeit (Sekunden) pro Element: 0.000004016
	(Umrechnungshilfe: Insgesamt 669.285931150 Minuten bzw. 11.154765519 Stunden)
	Ich habe 9999900000 RSA-Moduli untersucht.
	bye

# merker

	>>> import inflect
	>>> p=inflect.engine()
	>>> w=p.number_to_words(9999900000)
	>>> print(words)
	nine billion, nine hundred and ninety-nine million, nine hundred thousand

# base64

	Bin bei 99998 von 100000 (99.998000%) (min=5.693592).
	HIST 30 6.000000 62
	HIST 31 5.937500 683696474
	HIST 32 5.875000 8015560300
	HIST 33 5.812500 1292171582
	HIST 34 5.750000 8365397
	HIST 35 5.687500 6186
	HISTERR 0
	Bin bei 99999 von 100000 (99.999000%) (min=5.693592).
	HIST 30 6.000000 62
	HIST 31 5.937500 683703408
	HIST 32 5.875000 8015640462
	HIST 33 5.812500 1292184422
	HIST 34 5.750000 8365460
	HIST 35 5.687500 6186
	HISTERR 0
	Fertig, min = 5.693592.
	Benötigte Zeit: 44574.581386000 Sekunden, Zeit (Sekunden) pro Element: 0.000004458
	(Umrechnungshilfe: Insgesamt 742.909689767 Minuten bzw. 12.381828163 Stunden)
	Ich habe 9999900000 RSA-Moduli untersucht.
	bye

