#! /usr/bin/env bash


Anz=1000


if [ $(uname) = CYGWIN_NT-6.1 ]; then
    run=/cygdrive/d/git/rsa/keygen/ent-combined.py
else 
    run=$PWD/../ent-combined.py
fi

if [ ! -x $run ]; then
    echo ups
fi

for i in *; do 
    if [ -d $i ]; then
        (cd $i && $run) &
        sleep 1
    fi
done

echo Waiting for background jobs
wait
echo Bye

