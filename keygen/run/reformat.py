#! /usr/bin/env python3

import glob, sys
from collections import defaultdict

logfiles = glob.glob('./*/*.log', recursive=True)

plain=defaultdict(int); 
base64list=defaultdict(int);
base58list=defaultdict(int);
hexlist=defaultdict(int);

for i in logfiles:
    print("lese", i)
    with open(i,"rt") as f:
        for s in f:
            (name, index, value)=s.split()
            index=int(index)
            value=int(value)
            if index==0:
                sys.exit("ups")

            if name=='plain':
                plain[index]+=value
            elif name=='base64':
                base64list[index]+=value
            elif name=='base58':
                base58list[index]+=value
            elif name=='hex':
                hexlist[index]+=value
            else:
                sys.exit("error")


def output(name, data):

    print(name)
    summe_total=0;
    stepping=10

    for i in range(800,0,-1*stepping):
        summe=0
        for x in data:
            if (x>=i) and (x<i+stepping):
                #print("Grenze",i,"Wert",x)
                summe+=data[x]

        if summe>0:
            print(i, summe)
            summe_total+=summe

    print("total", summe_total)

output("plain", plain)
output("base64", base64list)
output("base58", base58list)
output("hex", hexlist)

