#! /usr/bin/env bash


if [ -z "$Anz" ]; then
    Anz=1000
fi


if [ $(uname) = CYGWIN_NT-6.1 ]; then
    gen=/cygdrive/d/git/rsa/keygen/GenPrimesDBfromSeed.exe
else 
    gen=$PWD/../GenPrimesDBfromSeed
fi

for i in *; do 
    if [ -d $i ]; then
        (cd $i && $gen $Anz $i) &
        sleep 1
    fi
done

echo Waiting for background jobs
wait
echo Bye

