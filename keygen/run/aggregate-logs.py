#! /usr/bin/env python3

import glob, sys

logfiles = glob.glob('./*/*.log', recursive=True)

plain=[ 0 ]*801

# https://www.afternerd.com/blog/python-copy-list/
base64list=plain.copy()
base58list=plain.copy()
hexlist=plain.copy()

for i in logfiles:
    print("lese", i)
    with open(i,"rt") as f:
        for s in f:
            (name, index, value)=s.split()
            index=int(index)
            value=int(value)
            if index==0:
                sys.exit("ups")

            if name=='plain':
                plain[index]+=value
            elif name=='base64':
                base64list[index]+=value
            elif name=='base58':
                base58list[index]+=value
            elif name=='hex':
                hexlist[index]+=value
            else:
                sys.exit("error")


def output(name, x):
    for i in range(800,0,-1):
        if x[i]>0:
            print(name, i, x[i])

output("plain", plain)
output("base64", base64list)
output("base58", base58list)
output("hex", hexlist)

