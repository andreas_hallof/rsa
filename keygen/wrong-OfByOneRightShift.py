#! /usr/bin/env python3

import math, pyfiglet
from KeyGenUtils import *

db=PrimeDB("primes.db"); print(db)
#Anz=db.getNumberOfElements()
Anz=1000
found=0; 
t=SmallPrimeFactorTest(Total=(Anz**2)-Anz)
print(t)
print(t.PrimesStr())
print("Ich teste {} Paare.".format(Anz**2-Anz))

widgets = [progressbar.Percentage(), ' ', progressbar.ETA(), ' ', progressbar.Bar()]
bar = progressbar.ProgressBar(widgets=widgets, max_value=(Anz)**2).start()
for a in range(0, Anz):
    for b in range(0, Anz):
        if a!=b:
            n=db.get(a)*db.get(b)
            n_encoded_1=n.to_bytes(bytes_needed(n), 'big')
            n_encoded_2=b'\x00\x00'+ n_encoded_1[1:-1]
            assert len(n_encoded_2)==256
            n_wrong=int.from_bytes(n_encoded_2, byteorder='big', signed=False)
            if t.test(n_wrong):
                found+=1

        bar.update(a*Anz + b)
bar.finish()

print("{} von {} ({:3.6f}%)".format(found, Anz**2-Anz, 100*found/float(Anz**2-Anz) ))

print(t.stats())
print(t.MissingPrimesStr())


