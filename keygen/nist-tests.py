#! /usr/bin/env python3

from hashlib import sha256
from binascii import hexlify
import secrets, math, sys, pyfiglet, shutil, base64, progressbar, platform
from gmpy2 import next_prime
from collections import Counter

import os, sys; 
sys.path.append( os.path.join(os.getcwd(), os.pardir, 'ent') )
from nistsp800_22 import *


def bytes_needed(n):
    if n == 0:
        return 1
    return int(math.log(n, 256)) + 1

def i2osp(integer, size=4):
  return ''.join([chr((integer >> (8 * i)) & 0xFF) for i in reversed(range(size))])

def banner(s):
    if not s:
        return 
    #rows, columns = os.popen('stty size', 'r').read().split()
    x=shutil.get_terminal_size((80, 24))
    print(pyfiglet.Figlet(font='cybermedium', width=int(x.columns)).renderText(s))

# experimenting with the entropy-measure-function from
# https://rosettacode.org/wiki/Entropy#Python
def entropy(s):
    p, lns = Counter(s), float(len(s))
    return -sum( count/lns * math.log(count/lns, 2) for count in p.values())

Anz=100; numbers=[]; proto_numbers=[];
print("Ich erzeuge {} 1024-Bit-Zahlen".format(Anz))
print("Plattform: ", platform.system())

widgets = [progressbar.Percentage(), progressbar.Bar()]
bar = progressbar.ProgressBar(widgets=widgets, max_value=Anz).start()
if platform.system()=='Linux' or platform.system().startswith('CYGWIN'):
    # being paranoid, i want to know where my randomness comes from
    with open("/dev/urandom", "rb") as f:
        for i in range(0,Anz):
            x=f.read(128)
            first=x[0] | 0x80
            x=first.to_bytes(1, 'big') + x[1:]
            p=int.from_bytes(x, byteorder='big', signed=False)
            proto_numbers.append(p)
            p=int(next_prime(p))
            numbers.append(p)
            assert bytes_needed(numbers[-1])==128
            bar.update(i + 1)
else:
    for i in range(0,Anz):
        numbers.append(secrets.randbits(1024))
        bar.update(i + 1)

bar.finish()


print("proto_primes:")
bar = progressbar.ProgressBar(widgets=widgets, max_value=Anz).start()
for a in proto_numbers:
    a_raw=a.to_bytes(bytes_needed(a), 'big') + \
            proto_numbers[-1].to_bytes(bytes_needed(proto_numbers[-1]), 'big')
    for func in [ monobit, block_frequency, independent_runs,
               longest_runs, matrix_rank, spectral, non_overlapping_patterns,
               serial, approximate_entropy, cumulative_sums ]:
        if func(a_raw) < 0.01:
            print("{:30}: {:.8f}".format( func.__name__, func(a_raw) ) )
    bar.update()
bar.finish()

print("primes:")
bar = progressbar.ProgressBar(widgets=widgets, max_value=Anz).start()
for a in numbers:
    a_raw=a.to_bytes(bytes_needed(a), 'big')
    for func in [ monobit, block_frequency, independent_runs,
               longest_runs, matrix_rank, spectral, non_overlapping_patterns,
               serial, approximate_entropy, cumulative_sums ]:
        if func(a_raw) < 0.01:
            print("{:30}: {:.8f}".format( func.__name__, func(a_raw) ) )
    bar.update()
bar.finish()

#sys.exit(0)

found=0; cnt=Counter()
#try:
if 1==1:
    print("Ich teste {} Paare".format((Anz-1)**2))
    bar = progressbar.ProgressBar(widgets=widgets, max_value=(Anz)**2).start()
    for a in range(0, Anz):
        for b in range(0, Anz):
            if a!=b:
                n=numbers[a]*numbers[b]
                #n=numbers[a]
                #print(hex(n))
                n_raw=n.to_bytes(bytes_needed(n), 'big')
                #print(hexlify(n_raw))
                for func in [ monobit, block_frequency, independent_runs,
                              longest_runs, matrix_rank, spectral, non_overlapping_patterns,
                              serial, approximate_entropy, cumulative_sums ]:
                    if func(n_raw) < 0.01:
                        print("{:30}: {:.8f}".format( func.__name__, func(n_raw) ) )
                        cnt[func.__name__]+=1
                        #raise ValueError("gefunden")
                        found+=1

            #print(a*Anz + b)
            bar.update(a*Anz + b)
    bar.finish()
    if found==0:
        print("alles mit ok durchgelaufen")
    else:
        print(found, "gefunden")
        print(cnt)

#except Exception as error:
#    #bar.finish()
#    print(error)
#
