#! /usr/bin/env python3

from hashlib import sha256
from binascii import hexlify
import math, pyfiglet, base64, base58
from KeyGenUtils import *
from collections import Counter


db=PrimeDB("primes.db"); print(db)
Anz=db.getNumberOfElements()
#Anz=10000
assert Anz<=db.getNumberOfElements()

tt=Anz**2-Anz
print("Ich teste {} Moduli ({}).".format(tt, math.log10(tt)))

minent=8
plain=[ 0 ]*801

# https://www.afternerd.com/blog/python-copy-list/
base64list=plain.copy()
base58list=plain.copy()
hexlist=plain.copy()

widgets = [progressbar.Percentage(), ' ', progressbar.ETA(), ' ', progressbar.AdaptiveTransferSpeed(), ' ', progressbar.Bar()]
bar = progressbar.ProgressBar(widgets=widgets, max_value=(Anz)**2).start()
for a in range(0, Anz):
    prime_a=db.get(a)
    for b in range(0, Anz):
        if a!=b:
            n=prime_a*db.get(b)
            n_bytes=n.to_bytes(256, byteorder='big')
            x=entropy(n_bytes)
            index=int(x*100)
            plain[index]+=1
            if x<minent:
                print("new minimal entropy", x)
                minent=x
            base64_1=base64.b64encode(n_bytes)
            x=entropy(base64_1[:256])
            index=int(x*100)
            base64list[index]+=1
            base58_1=base58.b58encode(n_bytes)
            x=entropy(base58_1[:256])
            index=int(x*100)
            base58list[index]+=1
            hexlist_1=hexlify(n_bytes)
            x=entropy(hexlist_1[:256])
            index=int(x*100)
            hexlist[index]+=1

        #bar.update(a*Anz + b)
    bar.update(a*Anz)
bar.finish()

def output(name, x):
    for i in range(800,0,-1):
        if x[i]>0:
            print(name, i, x[i])

    with open("output.log", "at") as f:
        for i in range(800,0,-1):
            if x[i]>0:
                s="{} {} {}\n".format(name, i, x[i])
                f.write(s)


output("plain", plain)
output("base64", base64list)
output("base58", base58list)
output("hex", hexlist)

