#include <stdio.h>
#include <gmp.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <assert.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>


/* https://techoverflow.net/2013/08/21/a-simple-mmap-readonly-example/
*/
size_t getFilesize(const char* filename)
{
    struct stat st;
    stat(filename, &st);
    return st.st_size;
}

 
int makehist(char *S, int *hist, int len) {
	int wherechar[256];
	int i, histlen;
	histlen=0;

	for(i=0;i<256;i++)
            wherechar[i]=-1;

	for(i=0;i<len;i++) {
		if (wherechar[(int)S[i]]==-1) {
			wherechar[(int)S[i]]=histlen;
			histlen++;
		}
		hist[wherechar[(int)S[i]]]++;
	}
	return histlen;
}
 
double entropy(int *hist, int histlen, int len) {
	int i;
	double H=0;

	for(i=0;i<histlen;i++) {
		H-= (double) hist[i]/len*log2( (double) hist[i]/len);
	}
	return H;
}
 
double entropy2(char *S, int len) {
        int table[256];
	int i;
	double H=0;

	for(i=0;i<256;i++)
            table[i]=0;
        for(i=0; i<len; i++) {
            //printf("i %d, S[i] %d\n", i, (unsigned char) S[i]);
            table[(unsigned char)S[i]]++;
        }

	for(i=0; i<256; i++) {
            if (table[i]>0) {
		H-= (double) table[i]/len*log2( (double) table[i]/len);
                // printf("H: %d\n", i);
            }
                
	}
	return H;
}


#define PRIMES_DB "primes.db"
int main(int argc, char** argv) {

    char S[]="1223334444";
    int len, *hist, histlen;
    double H;

    len=strlen(S);

    hist= (int*) calloc(len,sizeof(int));
    histlen=makehist(S, hist, len);
    H=entropy(hist, histlen, len);
    printf("%f\n", H);

    H=entropy2(S, len);
    printf("%f\n", H);

    clock_t zeit;
    mpz_t p, q, modulus;
    double min_ent=8.0, my_ent;
    char Modulus_Export[256];
    size_t countp;

    size_t filesize = getFilesize(PRIMES_DB);
    unsigned long int Anz=filesize/128;
    printf("Nr of primes in DB: %ld\n",Anz);

    int fd = open(PRIMES_DB, O_RDONLY, 0); assert(fd != -1);
    const char * mmappedData = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0); assert(mmappedData != MAP_FAILED);
    unsigned long int Elemente=Anz*Anz-Anz;
    printf("Ich teste %ld Moduli.\n", Elemente);

    mpz_inits(p, q, modulus, NULL);

    mpz_import(p, 128, 1, 1, 1, 0, &mmappedData[0]);
    if (mpz_probab_prime_p(p, 50)==0) {
        printf("upps, sanity prime-check failed\n");
        return 1;
    }

    printf("Los geht es.\n");
    /* https://www.gnu.org/software/libc/manual/html_node/CPU-Time.html
     */
    zeit=clock();

    for (unsigned long int a=0; a<Anz; a++) {
        for (unsigned long int b=0; b<Anz; b++) {
            if (a!=b) {
                mpz_import(p, 128, 1, 1, 1, 0, &mmappedData[a*128]);
                mpz_import(q, 128, 1, 1, 1, 0, &mmappedData[b*128]);
                mpz_mul(modulus, p, q);
                /* Function: void * mpz_export (
		 * void *rop, size_t *countp, int order, size_t size, int endian, size_t nails,
		 * const mpz_t op) */
                mpz_export(&Modulus_Export, &countp, 1, 1, 1, 0, modulus);
                my_ent=entropy2(Modulus_Export, 256);
                if (my_ent<min_ent) {
                    printf("new min %f\n", my_ent);
                    min_ent=my_ent;
                }
            }
        }
        printf("Bin bei %ld von %ld (%f%%) (min=%f).\n", a, Anz, (double) 100*a/Anz, min_ent);
    }



    zeit=clock() - zeit;
    double zeit_diff= ( (double) zeit) / CLOCKS_PER_SEC;
    printf("Fertig, min = %f.\n", min_ent);

    printf("Benötigte Zeit: %.9f Sekunden, Zeit (Sekunden) pro Element: %.9f\n", zeit_diff, zeit_diff / ((double) Elemente));
    printf("Ich habe %ld RSA-Moduli untersucht.\n", Elemente);

    close(fd);

    printf("bye\n");

    return 0;
}

