
// Prototypen
int base64_encode( 
        unsigned char *dst, size_t dlen, size_t *olen, const unsigned char *src,
        size_t slen );

int base64_encode_cut256( 
        unsigned char *dst, size_t dlen, size_t *olen, const unsigned char *src,
        size_t slen );

int base58_encode( 
        unsigned char *dst, size_t dlen, size_t *olen, const unsigned char *src,
        size_t slen );

int base58_encode_cut256( 
        unsigned char *dst, size_t dlen, size_t *olen, const unsigned char *src,
        size_t slen );

int hex_encode( 
        unsigned char *dst, size_t dlen, size_t *olen, const unsigned char *src,
        size_t slen );

int hex_encode_cut256( 
        unsigned char *dst, size_t dlen, size_t *olen, const unsigned char *src,
        size_t slen );

