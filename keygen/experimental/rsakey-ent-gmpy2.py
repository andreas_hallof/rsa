#! /usr/bin/env python3

from hashlib import sha256
from binascii import hexlify
import math, pyfiglet
from KeyGenUtils import *
from collections import Counter
from gmpy2 import mpz


db=PrimeDB("primes.db"); print(db)
Anz=db.getNumberOfElements()
#Anz=10000
assert Anz<=db.getNumberOfElements()

tt=Anz**2-Anz
print("Ich teste {} Moduli ({}).".format(tt, math.log10(tt)))

minent=8

widgets = [progressbar.Percentage(), ' ', progressbar.ETA(), ' ', progressbar.AdaptiveTransferSpeed(), ' ', progressbar.Bar()]
print("Importieren der Primzahlen (mem={})".format(MemUsage()))
bar = progressbar.ProgressBar(widgets=widgets, max_value=(Anz)**2).start()
prime_array=[]
for a in range(0, Anz):
    prime_array.append(mpz(db.get(a)))
bar.finish()
print("Done Import der Primzahlen (mem={})".format(MemUsage()))

print("Suche minimale Entropie")
bar = progressbar.ProgressBar(widgets=widgets, max_value=(Anz)**2).start()
for a in range(0, Anz):
    prime_a=prime_array[a];
    for b in range(0, Anz):
        if a!=b:
            n=int(prime_a*prime_array[b])
            n_bytes=n.to_bytes(256,byteorder='big')
            x=entropy(n_bytes)
            if x<minent:
                print("new minimal entropy", x)
                minent=x

        #bar.update(a*Anz + b)
    bar.update(a*Anz)
bar.finish()

