#! /usr/bin/env python3

from hashlib import sha256
from binascii import hexlify
import secrets, math, sys, pyfiglet, shutil, base64, progressbar, platform
from gmpy2 import next_prime
from collections import Counter

import os, sys; 
sys.path.append( os.path.join(os.getcwd(), os.pardir, 'ent') )
from nistsp800_22 import *


def bytes_needed(n):
    if n == 0:
        return 1
    return int(math.log(n, 256)) + 1

def i2osp(integer, size=4):
  return ''.join([chr((integer >> (8 * i)) & 0xFF) for i in reversed(range(size))])

def banner(s):
    if not s:
        return 
    #rows, columns = os.popen('stty size', 'r').read().split()
    x=shutil.get_terminal_size((80, 24))
    print(pyfiglet.Figlet(font='cybermedium', width=int(x.columns)).renderText(s))

# experimenting with the entropy-measure-function from
# https://rosettacode.org/wiki/Entropy#Python
def entropy(s):
    p, lns = Counter(s), float(len(s))
    return -sum( count/lns * math.log(count/lns, 2) for count in p.values())

class SmallPrimeFactorTest:
    def __init__(self, Total=-1, B=100):
        if Total<1:
            raise ValueError("you have to supply a valid 'Total'-value")
        self.Total=Total
        primes=[2, 3, 5, 7, 11, 13]
        while primes[-1]<B:
            x=next_prime(primes[-1])
            primes.append(x)
        self.primes=primes[:-1]
        self.cnt=Counter()

    def test(self, n):
        for p in self.primes:
            if n%p==0:
                self.cnt[p]+=1
                return True
        return False

    def __str__(self):
        return "Ich teste die ersten {} Primzahlen (max={}).".format(
            len(self.primes), self.primes[-1])

    def stats(self):
        res=""
        for (p,n) in self.cnt.most_common():
            res+="{}: {} ({:3.6f}%); ".format(p, n, 100*n/float(self.Total))

        return res

    def PrimesStr(self):
        return ", ".join(str(i) for i in self.primes)

    def MissingPrimesStr(self):
        res=""
        for i in self.primes:
            if not i in self.cnt:
                res+=" "+str(i)
        return res

Anz=99990000
#Anz=10

# https://progressbar-2.readthedocs.io/en/latest/examples.html
widgets = [progressbar.Percentage(), ' ', progressbar.ETA(), ' ', progressbar.Bar()]

found=0; found2=0;
t=SmallPrimeFactorTest(Total=Anz); print(t); print(t.PrimesStr())
u=SmallPrimeFactorTest(Total=Anz);
print("Ich teste {} Zahlen".format(Anz))
bar = progressbar.ProgressBar(widgets=widgets, max_value=Anz).start()
f=open("/dev/urandom", "rb")
for i in range(0, Anz):
    x=f.read(256); first=x[0] | 0x80; x=first.to_bytes(1, 'big') + x[1:]
    n=int.from_bytes(x, byteorder='big', signed=False)
    n_transposed=int.from_bytes(x, byteorder='little', signed=False)
    if t.test(n):
        found+=1
    if u.test(n_transposed):
        found2+=1
    bar.update(i)
f.close()
bar.finish()

print("direct    : {} von {} ({:3.6f}%)".format(found, Anz, 100*found/float(Anz) ))
print(t.stats())
print(t.MissingPrimesStr())
print("transposed: {} von {} ({:3.6f}%)".format(found2, Anz, 100*found/float(Anz) ))
print(u.stats())
print(u.MissingPrimesStr())



