#! /usr/bin/env python3

from hashlib import sha256
from binascii import hexlify
import secrets, math, sys, pyfiglet, shutil, base64, progressbar, platform
from gmpy2 import next_prime
from collections import Counter


def bytes_needed(n):
    if n == 0:
        return 1
    return int(math.log(n, 256)) + 1

def i2osp(integer, size=4):
  return ''.join([chr((integer >> (8 * i)) & 0xFF) for i in reversed(range(size))])

def banner(s):
    if not s:
        return 
    #rows, columns = os.popen('stty size', 'r').read().split()
    x=shutil.get_terminal_size((80, 24))
    print(pyfiglet.Figlet(font='cybermedium', width=int(x.columns)).renderText(s))

# experimenting with the entropy-measure-function from
# https://rosettacode.org/wiki/Entropy#Python
def entropy(s):
    p, lns = Counter(s), float(len(s))
    return -sum( count/lns * math.log(count/lns, 2) for count in p.values())

Anz=10**4; numbers=[]
print("Ich erzeuge {} 1024-Bit-Zahlen".format(Anz))
print("Plattform: ", platform.system())

widgets = [progressbar.Percentage(), progressbar.Bar()]
bar = progressbar.ProgressBar(widgets=widgets, max_value=Anz).start()
if platform.system()=='Linux' or platform.system().startswith('CYGWIN'):
    # being paranoid, i want to know where my randomness comes from
    with open("/dev/urandom", "rb") as f:
        for i in range(0,Anz):
            x=f.read(128)
            first=x[0] | 0x80
            x=first.to_bytes(1, 'big') + x[1:]
            numbers.append(int.from_bytes(x, byteorder='big', signed=False))
            assert bytes_needed(numbers[-1])==128
            bar.update(i + 1)
else:
    for i in range(0,Anz):
        numbers.append(secrets.randbits(1024))
        bar.update(i + 1)

bar.finish()


minimum=2**2048
print("Ich teste {} Paare".format((Anz-1)**2))
bar = progressbar.ProgressBar(widgets=widgets, max_value=(Anz)**2).start()
for a in range(0, Anz):
    for b in range(0, Anz):
        if a!=b:
            diff=abs(numbers[a]-numbers[b])
            if diff<2**100:
                print("gefunden")
            if diff<minimum:
                minimum=diff

        bar.update(a*Anz + b)
bar.finish()

print("Minimaler Abstand der aufgetreten ist: {} (log2={})".format(minimum, math.log2(minimum)))

