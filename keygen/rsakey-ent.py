#! /usr/bin/env python3

from hashlib import sha256
from binascii import hexlify
import math, pyfiglet
from KeyGenUtils import *
from collections import Counter


db=PrimeDB("primes.db"); print(db)
Anz=db.getNumberOfElements()
#Anz=10000
assert Anz<=db.getNumberOfElements()

tt=Anz**2-Anz
print("Ich teste {} Moduli ({}).".format(tt, math.log10(tt)))

minent=8

widgets = [progressbar.Percentage(), ' ', progressbar.ETA(), ' ', progressbar.AdaptiveTransferSpeed(), ' ', progressbar.Bar()]
bar = progressbar.ProgressBar(widgets=widgets, max_value=(Anz)**2).start()
for a in range(0, Anz):
    prime_a=db.get(a)
    for b in range(0, Anz):
        if a!=b:
            n=prime_a*db.get(b)
            n_bytes=n.to_bytes(256,byteorder='big')
            x=entropy(n_bytes)
            if x<minent:
                print("new minimal entropy", x)
                minent=x

        #bar.update(a*Anz + b)
    bar.update(a*Anz)
bar.finish()

