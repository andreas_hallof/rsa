#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "kodierungen.h"

int main(void) {
    
    unsigned char modulus[256], output[2000]={0};
    size_t olen;
    int res;

    for (int i=0; i<256; i++) modulus[i]=i;

    res=hex_encode_cut256(output, 2000, &olen, modulus, 256);
    printf("res=%d\n", res);

    printf("%s\n", output);

    return 0;
}
