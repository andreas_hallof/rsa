#! /usr/bin/env python3

import math, pyfiglet
from KeyGenUtils import *

Anz=10000

print("Memory:", MemUsage())

with open("primes.db", "ab") as f:
    for p in BatchGenPrimeNumbers(Anz):
        x=p.to_bytes(bytes_needed(p), 'big')
        assert len(x)==128
        f.write(x)

