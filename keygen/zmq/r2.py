#! /usr/bin/env python3


# https://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/multiprocess/multiprocess.html

import zmq
import time
import sys
from  multiprocessing import Process

def server(port="5556"):
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:%s" % port)
    print("Running server on port: ", port)

    while True:
        # Wait for next request from client
        message = socket.recv()
        print("Received request:", message)
        socket.send_string("World from {}".format(port))
         
def client(port="5556"):

    context = zmq.Context()
    print("Connecting to server with port", port)
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://localhost:{}".format(port))
    for request in range(20):
        print("Sending request ", request,"...")
        socket.send_string("Hello")
        message = socket.recv()
        print("Received reply ", request, "[", message, "]")
        #time.sleep(1) 


if __name__ == "__main__":

    Process(target=server).start()
        
    # Now we can connect a client to the server
    Process(target=client).start()
