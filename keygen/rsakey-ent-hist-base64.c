#include <stdio.h>
#include <gmp.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <assert.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>


/* https://techoverflow.net/2013/08/21/a-simple-mmap-readonly-example/
*/
size_t getFilesize(const char* filename)
{
    struct stat st;
    stat(filename, &st);
    return st.st_size;
}

double entropy2(char *S, int len) {
        int table[256];
	int i;
	double H=0;

	for(i=0;i<256;i++)
            table[i]=0;
        for(i=0; i<len; i++) {
            //printf("i %d, S[i] %d\n", i, (unsigned char) S[i]);
            table[(unsigned char)S[i]]++;
        }

	for(i=0; i<256; i++) {
            if (table[i]>0) {
		H-= (double) table[i]/len*log2( (double) table[i]/len);
                // printf("H: %d\n", i);
            }
                
	}
	return H;
}

/* mbedtls is your friend
 *
 * https://tls.mbed.org/base64-source-code
 *
 * */
static const unsigned char base64_enc_map[64] =
{
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
    'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
    'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
    'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
    'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7',
    '8', '9', '+', '/'
};

#define BASE64_SIZE_T_MAX   ( (size_t) -1 ) /* SIZE_T_MAX is not standard */

/*
 * Encode a buffer into base64 format
 */
int base64_encode( 
        unsigned char *dst, size_t dlen, size_t *olen, const unsigned char *src,
        size_t slen )
{
    size_t i, n;
    int C1, C2, C3;
    unsigned char *p;

    if( slen == 0 )
    {
        *olen = 0;
        return( 0 );
    }

    n = slen / 3 + ( slen % 3 != 0 );

    if( n > ( BASE64_SIZE_T_MAX - 1 ) / 4 )
    {
        *olen = BASE64_SIZE_T_MAX;
        //return( MBEDTLS_ERR_BASE64_BUFFER_TOO_SMALL );
        return( -1 );
    }

    n *= 4;

    if( ( dlen < n + 1 ) || ( NULL == dst ) )
    {
        *olen = n + 1;
        //return( MBEDTLS_ERR_BASE64_BUFFER_TOO_SMALL );
        return( -1 );
    }

    n = ( slen / 3 ) * 3;

    for( i = 0, p = dst; i < n; i += 3 )
    {
        C1 = *src++;
        C2 = *src++;
        C3 = *src++;

        *p++ = base64_enc_map[(C1 >> 2) & 0x3F];
        *p++ = base64_enc_map[(((C1 &  3) << 4) + (C2 >> 4)) & 0x3F];
        *p++ = base64_enc_map[(((C2 & 15) << 2) + (C3 >> 6)) & 0x3F];
        *p++ = base64_enc_map[C3 & 0x3F];
    }

    if( i < slen )
    {
        C1 = *src++;
        C2 = ( ( i + 1 ) < slen ) ? *src++ : 0;

        *p++ = base64_enc_map[(C1 >> 2) & 0x3F];
        *p++ = base64_enc_map[(((C1 & 3) << 4) + (C2 >> 4)) & 0x3F];

        if( ( i + 1 ) < slen )
             *p++ = base64_enc_map[((C2 & 15) << 2) & 0x3F];
        else *p++ = '=';

        *p++ = '=';
    }

    *olen = p - dst;
    *p = 0;

    return( 0 );
}

#define PRIMES_DB "primes.db"
int main(int argc, char** argv) {

    char S[]="1223334444";
    int len;
    double H;

    H=entropy2(S, len);
    printf("%f\n", H);

    clock_t zeit;
    mpz_t p, q, modulus;
    double min_ent=8.0, my_ent;
    char Modulus_Export[256];
    size_t countp;

    size_t filesize = getFilesize(PRIMES_DB);
    unsigned long int Anz=filesize/128;
    printf("Nr of primes in DB: %ld\n", Anz);

    int fd = open(PRIMES_DB, O_RDONLY, 0); assert(fd != -1);
    const char * mmappedData = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0); assert(mmappedData != MAP_FAILED);
    unsigned long int Elemente=Anz*Anz-Anz;
    printf("Ich teste %ld Moduli.\n", Elemente);

    mpz_inits(p, q, modulus, NULL);

    mpz_import(p, 128, 1, 1, 1, 0, &mmappedData[0]);
    if (mpz_probab_prime_p(p, 50)==0) {
        printf("upps, sanity prime-check failed\n");
        return 1;
    }

#define MAXBINS 40
    unsigned long int myhist[MAXBINS] = {0},
                      myhisterror = 0;
#define MAXENT 8.0
#define MINENT 5.5
    double myhistborders[MAXBINS+1];
    for (int i=0; i<MAXBINS+1; i++) {
        myhistborders[i]=MAXENT-((MAXENT-MINENT)/(double) MAXBINS)*(i+1);
        printf("%2d, %f\n", i, myhistborders[i]);
    }

    // mein buff muss bei 2048-Bit-Zahlen 349+1 Bytes groß sein.
    // ich nehme einfach ein paar byte mehr (schadet nicht).
    unsigned char base64_str[500]={0};
    size_t myolen;

    printf("Los geht es.\n");
    /* https://www.gnu.org/software/libc/manual/html_node/CPU-Time.html
     */
    zeit=clock();

    for (unsigned long int a=0; a<Anz; a++) {
        for (unsigned long int b=0; b<Anz; b++) {
            if (a!=b) {
                mpz_import(p, 128, 1, 1, 1, 0, &mmappedData[a*128]);
                mpz_import(q, 128, 1, 1, 1, 0, &mmappedData[b*128]);
                mpz_mul(modulus, p, q);
                /* Function: void * mpz_export (
		 * void *rop, size_t *countp, int order, size_t size, int endian, size_t nails,
		 * const mpz_t op) */
                mpz_export(&Modulus_Export, &countp, 1, 1, 1, 0, modulus);
                base64_encode(base64_str, 500, &myolen, Modulus_Export, 256);
                // printf("%d %s\n", myolen, base64_str);
                assert(myolen == 344);
                my_ent=entropy2(base64_str, 349);
                if (my_ent<min_ent) {
                    printf("new min %f\n", my_ent);
                    min_ent=my_ent;
                }
                for (int i=0; i<MAXBINS; i++) {
                    if (my_ent>myhistborders[i+1]) {
                        myhist[i]+=1;
                        goto C;
                    }
                }
                myhisterror+=1;
                C:
                1;
            }
        }
        printf("Bin bei %ld von %ld (%f%%) (min=%f).\n", a, Anz, (double) 100*a/Anz, min_ent);

        for (int i=0; i<MAXBINS; i++) {
            if (myhist[i]>0) {
		    printf("HIST %2d %f %ld\n", i, myhistborders[i+1], myhist[i]);
	    }
        }
        printf("HISTERR %ld\n", myhisterror);
    }



    zeit=clock() - zeit;
    double zeit_diff= ( (double) zeit) / CLOCKS_PER_SEC;
    printf("Fertig, min = %f.\n", min_ent);

    printf("Benötigte Zeit: %.9f Sekunden, Zeit (Sekunden) pro Element: %.9f\n", zeit_diff, zeit_diff / ((double) Elemente));
    if (zeit_diff>60) {
        printf("(Umrechnungshilfe: Insgesamt %.9f Minuten bzw. %.9f Stunden)\n",
                zeit_diff/60.0, zeit_diff/(3600.0) );
    }
    printf("Ich habe %ld RSA-Moduli untersucht.\n", Elemente);

    close(fd);

    printf("bye\n");

    return 0;
}

