#! /usr/bin/env python3

from hashlib import sha256
from binascii import hexlify
import math, pyfiglet, logging
from KeyGenUtils import *

generators = [
    (2, 11), (6, 13), (8, 17), (9, 19), (3, 37), (26, 53), (20, 61), (35, 71),
    (24, 73), (13, 79), (6, 97), (51, 103), (53, 107), (54, 109), (42, 127),
    (50, 151), (78, 157),
]

print(len(generators))

# Precalculate residues to speed up check
# -> calculate all possible n mod p where n ^ r mod p = 1
tests = []
for r, p in generators:
    l = []
    for i in range(p):
        if (i ** r) % p == 1:
            l.append(i)
    tests.append((p, set(l)))

print(tests)
print(len(tests))

zähler=nenner=1
for a, b in tests:
    zähler*=len(b)
    nenner*=a-1

print(zähler, nenner)
print(zähler/nenner)

# tests is equivalent to the original test masks,
# minus red herring entries with all bits set

# Equivalent to the original implementation
def is_vulnerable(modulus):
    return all(modulus % p in l for p, l in tests)

# Slower version, but using the generators directly with no precalc
def is_vulnerable_slower(modulus):
    return all(pow(modulus, r, p) == 1 for r, p in generators)

logging.basicConfig(filename="testrun-roca.log", level=logging.DEBUG, format='%(levelname)s: %(asctime)s: %(message)s')
db=PrimeDB("primes.db"); print(db)
Anz=db.getNumberOfElements()
#Anz=1000

tt=Anz**2-Anz
s="Ich teste {} Moduli ((log10) {}).".format(tt, math.log10(tt))
print(s); logging.info(s)

widgets = [progressbar.Percentage(), ' ', progressbar.ETA(), ' ', progressbar.AdaptiveTransferSpeed(), ' ', progressbar.Bar()]
bar = progressbar.ProgressBar(widgets=widgets, max_value=(Anz)**2).start()
for a in range(0, Anz):
    for b in range(0, Anz):
        if a!=b:
            n=db.get(a)*db.get(b)
            if is_vulnerable(n):
                s="ups gefunden n={:x}, p={:x}, q={:x}, a={:x}, b={:x}".format(n,
                            db.get(a), db.get(b), a, b)
                print(s); logging.info(s)

        #bar.update(a*Anz + b)
    bar.update(a*Anz)
bar.finish()

logging.info("Run (Anz={:x}) zu Ende.".format(Anz))

