#! /usr/bin/env python3

from hashlib import sha256
from binascii import hexlify
import secrets, math, sys, pyfiglet, shutil, base64, progressbar, platform
from gmpy2 import next_prime
from collections import Counter

import os, sys; 
sys.path.append( os.path.join(os.getcwd(), os.pardir, 'ent') )
from nistsp800_22 import *


def bytes_needed(n):
    if n == 0:
        return 1
    return int(math.log(n, 256)) + 1

def i2osp(integer, size=4):
  return ''.join([chr((integer >> (8 * i)) & 0xFF) for i in reversed(range(size))])

def banner(s):
    if not s:
        return 
    #rows, columns = os.popen('stty size', 'r').read().split()
    x=shutil.get_terminal_size((80, 24))
    print(pyfiglet.Figlet(font='cybermedium', width=int(x.columns)).renderText(s))

# experimenting with the entropy-measure-function from
# https://rosettacode.org/wiki/Entropy#Python
def entropy(s):
    p, lns = Counter(s), float(len(s))
    return -sum( count/lns * math.log(count/lns, 2) for count in p.values())

Anz=100; numbers=[]; #proto_numbers=[];
print("Ich erzeuge {} 2048-Bit-Zahlen".format(Anz))
print("Plattform: ", platform.system())

widgets = [progressbar.Percentage(), progressbar.Bar()]
bar = progressbar.ProgressBar(widgets=widgets, max_value=Anz).start()
if platform.system()=='Linux' or platform.system().startswith('CYGWIN'):
    # being paranoid, i want to know where my randomness comes from
    with open("/dev/urandom", "rb") as f:
        for i in range(0,Anz):
            x=f.read(256)
            first=x[0] | 0x80
            x=first.to_bytes(1, 'big') + x[1:]
            p=int.from_bytes(x, byteorder='big', signed=False)
            #proto_numbers.append(p)
            #p=int(next_prime(p))
            numbers.append(p)
            assert bytes_needed(numbers[-1])==256
            bar.update(i + 1)
else:
    for i in range(0,Anz):
        numbers.append(secrets.randbits(1024))
        bar.update(i + 1)

bar.finish()


print("2048-Bit-Zahlen:")
bar = progressbar.ProgressBar(widgets=widgets, max_value=len(numbers)).start()
for i in range(0,len(numbers)):
    a=numbers[i]
    a_raw=a.to_bytes(bytes_needed(a), 'big')
    bar.update(i)
bar.finish()

