#! /usr/bin/env python3

import secrets, math, sys, pyfiglet, shutil, base64, progressbar, platform
from gmpy2 import next_prime
from collections import Counter
from timeit import timeit

SETUP_CODE='''
with open("/dev/urandom", "rb") as f:
    x=f.read(256)
'''

print(
timeit( setup=SETUP_CODE,
        stmt='a=int.from_bytes(x, byteorder="little", signed=False)', 
        number=10**6 )
)

print(
timeit( setup=SETUP_CODE,
        stmt='a=int.from_bytes(x, byteorder="big", signed=False)', 
        number=10**6 )
)

B=5

primes=[3, 5 ]
while primes[-1]<B:
    x=next_prime(primes[-1])
    primes.append(x)

a=[0.5]

for i in primes:
    an=a[-1]+float(a[-1])/float(i)
    a.append(an)

print(a)

