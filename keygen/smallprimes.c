#include <stdio.h>
#include <gmp.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <assert.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>


/* https://techoverflow.net/2013/08/21/a-simple-mmap-readonly-example/
*/
size_t getFilesize(const char* filename)
{
    struct stat st;
    stat(filename, &st);
    return st.st_size;
}


#define PRIMES_DB "primes.db"
int main(int argc, char** argv) {

    clock_t zeit, zeit_t1;
    double zeit_diff;
    mpz_t p, q, modulus, r;
    char Modulus_Export[256];
    size_t countp;

    size_t filesize = getFilesize(PRIMES_DB);
    unsigned long int Anz=filesize/128;
    printf("Nr of primes in DB: %ld\n", Anz);
#define AnzPrimes 25
    unsigned int Primes[AnzPrimes]= { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 };

    int fd = open(PRIMES_DB, O_RDONLY, 0); assert(fd != -1);
    const char * mmappedData = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd, 0); assert(mmappedData != MAP_FAILED);
    unsigned long int Elemente=Anz*Anz-Anz;
    printf("Ich teste %ld Moduli.\n", Elemente);

    mpz_inits(p, q, modulus, r, NULL);

    mpz_import(p, 128, 1, 1, 1, 0, &mmappedData[0]);
    if (mpz_probab_prime_p(p, 50)==0) {
        printf("upps, sanity prime-check failed\n");
        return 1;
    }

    printf("Los geht es.\n");
    /* https://www.gnu.org/software/libc/manual/html_node/CPU-Time.html
     */
    zeit=clock();

    for (unsigned long int a=0; a<Anz; a++) {
        for (unsigned long int b=0; b<Anz; b++) {
            if (a!=b) {
                mpz_import(p, 128, 1, 1, 1, 0, &mmappedData[a*128]);
                mpz_import(q, 128, 1, 1, 1, 0, &mmappedData[b*128]);
                mpz_mul(modulus, p, q);
                /* Function: void * mpz_export (
		 * void *rop, size_t *countp, int order, size_t size, int endian, size_t nails,
		 * const mpz_t op) */
                mpz_export(&Modulus_Export, &countp, 1, 1, 1, 0, modulus);
#define METHODE1 YES
#ifdef METHODE1
                /* https://gmplib.org/manual/Integer-Division.html#Integer-Division */
                for (unsigned int i=0; i<AnzPrimes; i++) {
                    if (mpz_divisible_ui_p(modulus, Primes[i])>0) {
                        printf("kann nicht sein.\n");
                        return 1;
                    }
                }
#endif
#ifdef METHODE2
                /* https://gmplib.org/manual/Integer-Division.html#Integer-Division */
                for (unsigned int i=0; i<AnzPrimes; i++) {
                    mpz_mod_ui(r, modulus, Primes[i]);
                    if (mpz_cmp_ui(r,0)==0) {
                        printf("kann nicht sein.\n");
                        return 1;
                    }
                }
#endif
            }
        }
        if (a%10==0) {
            double p=(double) a/Anz;
            printf("Bin bei %ld von %ld (%f%%).\n", a, Anz, 100.0*p);
            zeit_t1=clock() - zeit;
            zeit_diff= (( (double) zeit_t1) / CLOCKS_PER_SEC )*(1-p)/p;
            printf("ETA %fd %fh, %fm, %fs\n", zeit_diff/(3600.0*24.0), zeit_diff/3600.0, zeit_diff/60.0, zeit_diff);
        }
    }



    zeit=clock() - zeit;
    zeit_diff= ( (double) zeit) / CLOCKS_PER_SEC;
    printf("Fertig.\n");

    printf("Benötigte Zeit: %.9f Sekunden, Zeit (Sekunden) pro Element: %.9f\n", zeit_diff, zeit_diff / ((double) Elemente));
    if (zeit_diff>60) {
        printf("(Umrechnungshilfe: Insgesamt %.9f Minuten bzw. %.9f Stunden)\n",
                zeit_diff/60.0, zeit_diff/(3600.0) );
    }
    printf("Ich habe %ld RSA-Moduli untersucht.\n", Elemente);

    close(fd);

    printf("bye\n");

    return 0;
}

